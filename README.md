# DOCUMENTATION : COGNITIVE SYSTEMS USING EYE TRACKING

## 
Author

* **Rahma Hellali**  

School : Higher Management Institute of Tunis
Email : hellalirahma95@gmail.com
Period : February-August 2019


Modified : 20/12/2019
## Introduction

This file will guide you to run the project.

It's a project that implements gaze tracking in order to help disabled people to express their needs by gazing.


## Files

The code directory contains files for gaze tracking and files for reinforcement learning 

### Gaze Tracking 
part
Code:
	
	-gaze_main.py : This code presents the main of the project. This is the file to run.
	
	-gaze_gui.py : This code implements the differents componenets of the GUI
	
	-gaze_process.py : This code defines the processing functions : 
		these functions are responsable of the detection of the gaze and its classification .
		
Images : (pictures used in the project )
	
			-person_1.jpg
		
			-person_2.jpg

		Training file: (file used for landmarks detection)
	
	-shape_predictor_68_face_landmarks.dat 
	
		(to download from https://github.com/AKSHAYUBHAT/TensorFace/blob/master/openface/models/dlib/shape_predictor_68_face_landmarks.dat)



### Prerequisites

It is better that the python version is 3.5


	Download the following file : shape_predictor_68_face_landmarks.dat, and move it to the code directory.

	```
Download Link : https://github.com/AKSHAYUBHAT/TensorFace/blob/master/openface/models/dlib/shape_predictor_68_face_landmarks.dat
	
```
Change the path in the line os.chdir([PATH]) by the path of the code directory.

This line should be changed in the following files : Prog.py , Config.py and Environment.py .

	```
Example : os.chdir("C:/Users/Alex/Downloads/gaze_tracking_code_karim/code")

	```
Download the following Python libraries : imutils, dlib, cv2 (opencv version 4), numpy, time, PyQt5, datetime, sys, pandas, scipy, matplotlib and (PyUserInput or win32api) 

	```
Example command : pip install imutils dlib cv2 numpy time PyQt5 datetime sys pandas scipy matplotlib PyUserInput win32api
```


### Run the project


##Preparations : 
Before running the project make sure that:
	
	- You sit at distance of 25-30 cm of the screen
	
	- Your face should be parallal at the center of the screen
	
	- You don't wear glasses
	
	- There are good light conditions.


## RUN

To run th project compile the gaze_main.py file.

 ```
Example command : python gaze_main.py
```

