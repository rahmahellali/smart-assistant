import pandas as pd
import random
from Config import *
from Environment import *
from Classifier import *
from ClassifierSet import *
import Config


class MatchSet(ClassifierSet):
    def __init__(self,pop,env,actual_time):
        ClassifierSet.__init__(self,env,actual_time)
        self.pop = pop
        self.ecart_time = 30
        self.ecart_temperature = 2
        self.ecart_luminosity = 10
        self.ecart_state = 0.2
        
        
        for cl in self.pop.clSet:
            if self.does_match(cl):
                self.clSet.append(cl)

        initial_action = 0
        while self.num_of_different_actions() < conf.min_number_action_in_MatchSet:
            cond = []
            clm = Classifier(cond,actual_time)
            for i in range(len(self.env.state)):
                if i==0:
                    hour_min=self.decrease_Time(str(self.env.state[i]),self.ecart_time)
                    hour_max=self.add_Time(str(self.env.state[i]),self.ecart_time)
                    cond.append(hour_min)
                    cond.append(hour_max)
                if i==1:
                    temperature_room_min=self.decrease_Number(env.state[i],self.ecart_temperature)
                    temperature_room_max=self.add_Number(env.state[i],self.ecart_temperature)
                    cond.append(int(temperature_room_min))
                    cond.append(int(temperature_room_max))
                if i==2:
                    temperature_exterior_min=self.decrease_Number(env.state[i],self.ecart_temperature)
                    temperature_exterior_max=self.add_Number(env.state[i],self.ecart_temperature)
                    cond.append(int(temperature_exterior_min))
                    cond.append(int(temperature_exterior_max))
                if i==3:
                    luminosity_min=self.decrease_Number(env.state[i],self.ecart_luminosity)
                    luminosity_max=self.add_Number(env.state[i],self.ecart_luminosity)
                                    
                    if luminosity_min < 0:
                        luminosity_min = 0
                        cond.append(int(luminosity_min))
                        cond.append(int(luminosity_max))

                    elif luminosity_max > 100 :
                        luminosity_max = 100
                        cond.append(int(luminosity_min))
                        cond.append(int(luminosity_max))                    
                    else:
                        cond.append(int(luminosity_min))
                        cond.append(int(luminosity_max))

                if i==4:

                    state_window_min=self.decrease_Number(env.state[i],self.ecart_state)
                    state_window_max=self.add_Number(env.state[i],self.ecart_state)

                    if state_window_min < 0:
                        state_window_min = 0
                        cond.append(round(state_window_min,2))
                        cond.append(round(state_window_max,2))

                    elif state_window_max > 3 :
                        state_window_max = 3
                        cond.append(round(state_window_min,2))
                        cond.append(round(state_window_max,2))
                    else:
                        cond.append(round(state_window_min,2))
                        cond.append(round(state_window_max,2))
                    
                if i==5:
                    state_bed_min=self.decrease_Number(env.state[i],self.ecart_state)
                    state_bed_max=self.add_Number(env.state[i],self.ecart_state)

                    if state_bed_min < 0:
                        state_bed_min = 0
                        cond.append(round(state_bed_min,2))
                        cond.append(round(state_bed_max,2))
                    elif state_bed_max > 3 :
                        state_bed_max = 3
                        cond.append(round(state_bed_min,2))
                        cond.append(round(state_bed_max,2))
                    else:
                        cond.append(round(state_bed_min,2))
                        cond.append(round(state_bed_max,2))

            clm.condition = cond
            clm.action = initial_action
            clm.experience = 0
            clm.time_stamp = actual_time
            clm.action_set_size = self.numerosity_sum() + 1
            clm.numerosity = 1
            self.pop.clSet.append(clm)
            self.clSet.append(clm)
            initial_action+=1
            while self.pop.numerosity_sum() > conf.Max_size_population:
                cl_del = pop.delete_from_population()
                if cl_del == None:
                    if cl_del in self.clSet:
                        self.clSet.remove(cl_del)



#    def does_match(self,cl):
#        if cl.condition[0] <= self.env.state[0] <= cl.condition[1]:
#            return True
#        return False


#    def does_match(self,cl):
#        if (cl.condition[0] <= self.env.state[0] < cl.condition[1]) and (cl.condition[2] <= self.env.state[1] < cl.condition[3]) and (cl.condition[4] <= self.env.state[2] < cl.condition[5]) and (cl.condition[6] <= self.env.state[3] < cl.condition[7]) and (cl.condition[8] <= self.env.state[4] < cl.condition[9]) and (cl.condition[10] <= self.env.state[5] < cl.condition[11]):
##        if ( str(self.env.state[0]) >= str(cl.condition[0]) and str(self.env.state[0]) < str(cl.condition[1])) and ( self.env.state[1] >= float (cl.condition[2]) and self.env.state[1] < float(cl.condition[3])):
#            return True
#        return False
    def does_match(self,cl):
        if (cl.condition[0] <= self.env.state[0] < cl.condition[1]) and (cl.condition[2] <= self.env.state[1] < cl.condition[3]) and (cl.condition[4] <= self.env.state[2] < cl.condition[5]) and (cl.condition[6] <= self.env.state[3] < cl.condition[7]):
            return True
        return False

#    def does_match(self,cl):
#        
#        if "07:00"<= self.env.state[0] <="17:00" and 21 <= self.env.state[2] <= 26 and 0.0 <= self.env.state[4] <= 1.0:
#            if (cl.condition[0] <= self.env.state[0] < cl.condition[1]) and (cl.condition[4] <= self.env.state[2] < cl.condition[5]) and (cl.condition[8] <= self.env.state[4] < cl.condition[9]):
#                return True
#        
#        if "17:00" <= self.env.state[0] <= "19:00" and 15 <= self.env.state[2] <= 17 and 1.0 <= self.env.state[4] <= 2.0:
#            if (cl.condition[0] <= self.env.state[0] < cl.condition[1]) and (cl.condition[4] <= self.env.state[2] < cl.condition[5]) and (cl.condition[8] <= self.env.state[4] < cl.condition[9]):
#                return True
#
#        if "11:00"<= self.env.state[0] <= "12:00":
#            if (cl.condition[0] <= self.env.state[0] < cl.condition[1]):
#                return True
#        
#        if 1.0 <= self.env.state[5] <= 3.0  :
#            if (cl.condition[10] <= self.env.state[5] < cl.condition[11]):
#                return True
#        
#        if 21 <= self.env.state[1]<= 29 or 14 <= self.env.state[1]<= 17:
#            if (cl.condition[2] <= self.env.state[1] < cl.condition[3]) and (cl.condition[4] <= self.env.state[2] < cl.condition[5]):
#                return True
#               
#        else:
#            if (cl.condition[0] <= self.env.state[0] < cl.condition[1]) and (cl.condition[2] <= self.env.state[1] < cl.condition[3]) and (cl.condition[4] <= self.env.state[2] < cl.condition[5]) and (cl.condition[6] <= self.env.state[3] < cl.condition[7]):
#                return True
#        return False

    def num_of_different_actions(self):
        a_list = []
        for cl in self.clSet:
            a_list.append(cl.action)
        return len(set(a_list))

    def random_action(self):
        return (random.randrange(Config.Actions_count))


    def add_Time(self,time,time_plus):
        time=str(time)
        minute_part = time[3] + time[4]
        hour_part = time[0] + time[1]
        minute = int(minute_part) + time_plus
        if minute >= 60:
            minute = minute-60
            hour = int(hour_part)+1
        else:
            hour = int(hour_part)
        if hour < 10:
            hour = "0" + str(hour)
        if minute < 10:
            minute = "0" + str(minute)
        timeAdded = str(hour) + ":" + str(minute)
        return timeAdded
    
    
    def decrease_Time(self,time,time_d):
        time=str(time)
        minute_part = time[3] + time[4]
        hour_part = time[0] + time[1]
        minute = int(minute_part) - time_d
        if minute < 0:
            minute = 60-abs(minute)
            hour = int(hour_part)-1
        else:
            hour = int(hour_part)
        if hour < 10:
            hour = "0" + str(hour)
        if minute < 10:
            minute = "0" + str(minute)
        timeDecreased = str(hour) + ":" + str(minute)
        return timeDecreased


    def add_Number(self,val,rand_num):
        new_val= val + rand_num
        return new_val


    def decrease_Number(self,value,number):
        decrease_value = value - number
        return decrease_value