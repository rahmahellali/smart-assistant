"""

"""
##Importing libraries
#GUI libraries
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.uic import loadUi
#Detection librariess
import cv2
import dlib
import numpy as np
from imutils import face_utils
#Time libraries
import time
import datetime
import sys
from Config import *

#Importing functions and code from the GUI file and the Process file
from map_face_gui_mod_ui import Ui_Main
import map_face_process as process
from Prog import Prog

class MainWindow(QMainWindow, Ui_Main, Prog):
    def __init__(self, parent=None):
        """
        This class set the GUI interface and impelemnt the gaze tracking functions
        """
        #Importing the GUI components
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        #Screen dimensions
        self.screen_width = process.screen_width #1296 #640
        self.screen_height = process.screen_height #698 #1361

        #Set the listeners for each stack
        self.process_stack_1()
        self.process_stack_2()
        self.process_stack_3()
        self.process_stack_4()
        self.process_stack_5()
        self.process_stack_6()
        self.process_stack_7()
        self.process_stack_8()

        #Initialize "Edit parameters" section variables'
        self.dwelling_time = 2
        self.question_time = 2
        self.dwelling_lineEdit.setText(str(self.dwelling_time))
        self.question_time_lineEdit.setText(str(self.question_time))
        self.question_show_activated = False #used to maintain image show

        #Camera's state boolean : indicates if the camera is running
        self.camera_is_running = False

        #Initial variables for detection and choice making
        self.detector, self.predictor, self.detector_pupil = process.init_cv()
        self.LEFT_EYE_POINTS, self.RIGHT_EYE_POINTS = process.init_var_eye()
        self.questions_answered, self.question_current, self.question_index, self.questions_number, self.choice_index, self.choices, self.choices_map,\
        self.text_left, self.text_right ,self.choices, self.choices_images, self.image_left, self.image_right, self.duration,\
        self.questions_names, self.questions_images,self.questions_choices, self.questions_next,\
        self.questions_choices_notifications = process.init_var_choices()
        
        ##Booleans for detection
        #Indicates if there is a gaze detection in the previous frame
        self.gaze_detected_previous = None
        #Variables for measuring the period of gazing at the same choice 
        self.start, self.end = None, None

        ##Inital variables for calibrating
        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
        self.nb_frames = 10
        
        #Inital variables for the "Ask a question" section
        self.question = ""
        self.choice_1 = ""
        self.choice_2 = ""
        self.choice_selected = None  

        #Left and right limits for gaze classifying
        self.l_limit, self.r_limit = 0.5, 0.47

        #Initial variables for notifications
        self.notification_previous = False
        self.notification = None           

        #For saving coputational resources
        ##Number of frame to process = 1 for each self.frames_to_process frames
        self.frames_to_process = 1
        self.frames_processed = 0

        #Initial variables for automatic mode
        self.questions_counter_auto, self.questions_number_auto, self.question_index, self.choice_index,\
        self.choices, self.choices_map,\
        self.choices_images, self.duration,\
        self.questionAuto, self.stateAuto,self.notificationAuto = self.init_auto()

        #Initial variables for solution2 mode
        self.questions_counter_sol2, self.questions_number_sol2, self.question_index_sol2,self.question_number_selection ,self.questions_counter_selection,self.choice_index,\
        self.choices, self.choices_map,\
        self.choices_images, self.duration,\
        self.questionSol2, self.stateSol2,self.notificationAuto = self.init_auto_solution2()
        #
        Prog.__init__(self)

    #Set listeners for each stack
    def process_stack_1(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_1")
        self.patientButton.clicked.connect(self.patientButton_onClick)
        self.assistantButton.clicked.connect(self.assistantButton_onClick)

    def process_stack_2(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_2")
        self.mainButton_1.clicked.connect(self.mainButton_OnClick)
        self.guidedButton.clicked.connect(self.guidedButton_OnClick)
        self.automaticButton.clicked.connect(self.automaticButton_OnClick)
        self.solution2Button.clicked.connect(self.solution2Button_OnClick)

    def process_stack_3(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_3")
        self.mainButton_2.clicked.connect(self.mainButton_OnClick_guided)
        self.startButton.clicked.connect(self.init_var_guided)
        self.startButton.clicked.connect(self.start_webcam_guided)
        self.nextButton.clicked.connect(self.next_guided_OnClick)
        self.stopButton.clicked.connect(self.stop_webcam_guided)

    def process_stack_4(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_4")
        self.mainButton_3.clicked.connect(self.mainButton_OnClick)
        self.askButton.clicked.connect(self.askButton_OnClick)
        self.editButton.clicked.connect(self.editButton_OnClick)

    def process_stack_5(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_5")
        self.mainButton_ask.clicked.connect(self.mainButton_ask_OnClick)
        self.startButton_ask.clicked.connect(self.start_webcam_ask)

    def process_stack_6(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_6")
        self.submit_parameters_Button.clicked.connect(self.submit_parameters_Button_OnClick)
        self.mainButton_edit.clicked.connect(self.main_edit_Button_OnClick)

    def process_stack_7(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_7")
        self.mainButtonAuto.clicked.connect(self.mainButton_OnClick_auto)
        self.startButtonAuto.clicked.connect(self.init_var_auto)
        self.startButtonAuto.clicked.connect(self.start_webcam_auto)
        self.nextButtonAuto.clicked.connect(self.next_auto_OnClick)
        self.stopButtonAuto.clicked.connect(self.stop_webcam_auto)
    
    def process_stack_8(self):
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_8")
        self.mainButtonAuto8.clicked.connect(self.mainButton_OnClick_auto)
        self.startButtonAuto8.clicked.connect(self.init_var_soltuion2)
        self.startButtonAuto8.clicked.connect(self.start_webcam_solution2)
        self.nextButtonAuto8.clicked.connect(self.next_soltuion2_OnClick)
        self.stopButtonAuto8.clicked.connect(self.stop_webcam_auto_solution2)

    # Main
    def patientButton_onClick(self):
        """
        The Patient button listener : going to the Patient stack
        """
        self.QtStack.setCurrentIndex(1)
    
    def assistantButton_onClick(self):
        """
        The Assistant button listener : going to the Assistant stack
        """
        self.QtStack.setCurrentIndex(3)
    
    def mainButton_OnClick(self):
        """
        The Main button listener : getting back to the Main stack
        """
        self.QtStack.setCurrentIndex(0)

    #Patient
    def guidedButton_OnClick(self):
        """
        The Guided button listener : going to the Guided Mode stack
        """
        self.QtStack.setCurrentIndex(2)

    def automaticButton_OnClick(self):
        """
        The Automatic button listener : going to the Automatic Mode stack
        """
        self.QtStack.setCurrentIndex(6)

    def solution2Button_OnClick(self):
        """
        The Automatic button listener : going to the Automatic Mode stack
        """
        self.QtStack.setCurrentIndex(7)

    ##Guided Mode stack functions
    def init_var_guided(self):
        """
        Initialize variables for guided mode
        """
        self.camera_is_running = False

        self.questions_answered, self.question_current, self.question_index, self.questions_number, self.choice_index, self.choices, self.choices_map,\
        self.text_left, self.text_right ,self.choices, self.choices_images, self.image_left, self.image_right, self.duration,\
        self.questions_names, self.questions_images,self.questions_choices, self.questions_next,\
        self.questions_choices_notifications = process.init_var_choices()
        
        self.gaze_detected_previous = None
        self.start, self.end = None, None

        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
        
        self.notification_previous = False
        self.notification = None 

    def start_webcam_guided(self):
        """
        The Start button listener:
            *activate the camera
            *run the processing function "update_frame" in a loop mode
        """
        if not self.camera_is_running:
            #Running the camera
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            #Setting the time : the class responsable for running the "processing function" in while loop mode
            self.timer = QtCore.QTimer(self)
            ##Setting the timer function
            self.timer.timeout.connect(self.update_frame)
            ##Start the timer
            self.timer.start(2)

    #Next button
    def init_var_guided_next(self):
        """
        Initialize variables for Next button in the guided mode
        """
        self.start, self.end = None, None
        self.duration = 0
        self.gaze_detected_previous = None

        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
    
    def next_guided_OnClick(self):
        """
        Next buttton on click listener:
            *initialize parameters
            *activate the camera
        """
        if self.notification_previous and not self.camera_is_running:
            #Initialize parameters
            self.init_var_guided_next()
            #Activate the camera
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            self.notification_previous = False
            self.notification = None     

    def notification_activated(self):
        """
        Stop the camera when a choice, that is followed by a notification, is selected
        """
        self.camera_is_running = False
        self.capture.release()

    def stop_webcam_guided(self):
        """
        The Start button listener:
            *desactivate the camera
            *reinitializing the boars
        """
        if self.camera_is_running:
            #Desactivate the camera
            self.capture.release()
            self.timer.stop()
            self.camera_is_running = not self.camera_is_running
            #Reinitializing the boars
            self.board = process.init_board(self.screen_height, self.screen_width)
            self.display_image(self.board, "stack_3")
   
    def mainButton_OnClick_guided(self):
        """
        The Main button listener : 
            *getting back to the Main stack
            *stopping teh camera
        """
        self.QtStack.setCurrentIndex(0)
        self.stop_webcam_guided()

    #Assistant
    def askButton_OnClick(self):
        """
        The "Ask a question" button listener : going to the "Ask a question" section stack
        """
        self.board = process.init_board(self.screen_height, self.screen_width)
        self.display_image(self.board, "stack_5")
        self.QtStack.setCurrentIndex(4)
    
    def editButton_OnClick(self):
        """
        The "Edit parameters" button listener : going to the "Edit parameters" section stack
        """
        self.QtStack.setCurrentIndex(5)

    # "Ask a question" section
    def init_var_choices_ask(self):
        """
        Initalize choices variables for the "Ask a question" section
        """
        self.text_left = self.choice_1
        self.text_right = self.choice_2
        self.choices[1] = self.text_left
        self.choices[2] = self.text_right
        self.choice_selected = None

    def init_var_ask(self):
        """
        Initalize choices variables for the "Ask a question" section
        """
        self.camera_is_running = False
        self.init_var_choices_ask()
        self.gaze_detected_previous = None
        self.start, self.end = None, None
        self.duration = 0
        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
        
    def start_webcam_ask(self):
        """
        The Start button listener in the "Ask a question" section:
            *Get the question and choices written by the assistant
            *Processing:
                -show the question
                -detect the patient's choice
        """
        self.question = self.question_lineEdit.text()
        self.choice_1 = self.choice_1_lineEdit.text()
        self.choice_2 = self.choice_2_lineEdit.text() 
        if (not self.camera_is_running) and (self.question != "") and (self.choice_1 != "") and (self.choice_2 != ""):
            #Show the question
            self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
            process.write_text(self.question, cv2.FONT_HERSHEY_COMPLEX_SMALL, 3, 4, self.screen_width, int(self.screen_height), 0, 0, self.board)
            self.display_image(self.board, "stack_5")
            self.question_show_activated =True
            #Intitalize variables
            self.init_var_ask()
            #Activate the camera
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            #Set the timer
            self.timer_ask = QtCore.QTimer(self)
            self.timer_ask.timeout.connect(self.update_frame_ask)
            self.timer_ask.start(2)

    def stop_webcam_ask(self):
        """
        The Stop button listener in the "Ask a question" section:
            *Desactivate the camera
            *Stop the timer
            *Display the choice made by the patient
            *Reintializing variables
        """
        if self.camera_is_running:
            #Desactivate the camera
            self.capture.release()
            #Stop the timer
            self.timer_ask.stop()
            self.camera_is_running = not self.camera_is_running
            #Display the choice made by the patient
            self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
            process.write_text(self.choice_selected, cv2.FONT_HERSHEY_COMPLEX_SMALL, 3, 4, self.screen_width, self.screen_height, 0, 0, self.board)
            self.display_image(self.board, "stack_5")
            #Reintializing variables
            self.question_lineEdit.setText("")
            self.choice_1_lineEdit.setText("")
            self.choice_2_lineEdit.setText("")
            self.question = ""
            self.choice_1 = ""
            self.choice_2 = ""
            self.choice_selected = None
    
    def stop_webcam_ask_main(self):
        """
        Stop the camera when the Main button is pushed
            *Desactivate the camera
            *Stop the timer
            *Reinilize the board
            *Reintialize variables
        """
        if self.camera_is_running:
            #Desactivate the camera
            self.capture.release()
            #Stop the timer
            self.timer_ask.stop()
            self.camera_is_running = not self.camera_is_running
            #Reinilize the board
            self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
            #Reintializing variables
            self.question_lineEdit.setText("")
            self.choice_1_lineEdit.setText("")
            self.choice_2_lineEdit.setText("")
            self.question = ""
            self.choice_1 = ""
            self.choice_2 = ""
            self.choice_selected = None

    def mainButton_ask_OnClick(self):
        """
        The Main button listener in the "Ask a question" section : 
            *getting back to the Main stack
            *stopping teh camera
        """
        self.QtStack.setCurrentIndex(0)
        self.stop_webcam_ask_main()

    # "Edit Paramaters" section
    def submit_parameters_Button_OnClick(self):
        """
        The Submit button listener : changing the parameters 
        values with the new ones written by the assistant 
        """
        dwellling_text = self.dwelling_lineEdit.text()
        question_time_text = self.question_time_lineEdit.text()
        if dwellling_text != "":
            self.dwelling_time = int(dwellling_text)
        if question_time_text != "":
            self.question_time = int(question_time_text)
    
    def main_edit_Button_OnClick(self):
        """
        The Main button listener : getting back to the Main stack
        """
        self.QtStack.setCurrentIndex(0)

    #Display image
    def display_image(self, img, window):
        """
        Function responsable for showing the board in each stack
        """
        # Makes OpenCV images displayable on PyQT
        qformat = QtGui.QImage.Format_Indexed8
        if len(img.shape) == 3:
            if img.shape[2] == 4:
                qformat = QtGui.QImage.Format_RGBA8888
            else:
                qformat = QtGui.QImage.Format_RGB888
        out_image = QtGui.QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        out_image = out_image.rgbSwapped()
        # Display the image 
        if window == "stack_1":
            self.baseImage_1.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_1.setScaledContents(True)
        if window == "stack_2":
            self.baseImage_2.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_2.setScaledContents(True)
        if window == "stack_3":
            self.baseImage_3.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_3.setScaledContents(True)
        if window == "stack_4":
            self.baseImage_4.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_4.setScaledContents(True)
        if window == "stack_5":
            self.baseImage_ask.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_ask.setScaledContents(True)
        if window == "stack_6":
            self.baseImage_edit.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_edit.setScaledContents(True)
        if window == "stack_7":
            self.baseImage_auto.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_auto.setScaledContents(True)
        if window == "stack_8":
            self.baseImage_auto8.setPixmap(QtGui.QPixmap.fromImage(out_image))
            self.baseImage_auto8.setScaledContents(True)

    def projection(self, horizontal_ratio):
        """
        Project the gaze on the board:
            *Draw a circle at the estimated gaze position
        """
        if horizontal_ratio and not self.notification_previous and self.camera_is_running:
            #Show the horizontal ratio value
            cv2.putText(self.board, "{:03.2f}".format(horizontal_ratio),(548,50),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
            #Show the gaze position value
            point_of_gaze_position =(1-(horizontal_ratio))*self.screen_width
            cv2.putText(self.board, "{:03.2f}".format(point_of_gaze_position),(548,150),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
            cv2.putText(self.board, "{:03.2f}".format(self.screen_width/2),(548,100),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
            #Draw a circle
            cv2.circle(self.board, (int(point_of_gaze_position), int(self.screen_height/2)), 20, (0,0, 255), -1)
            cv2.circle(self.board, (int(point_of_gaze_position), int(self.screen_height/2)), 2, (0,0, 0), -1)
            #Draw the center of the screen
            cv2.circle(self.board, (int(self.screen_width/2), int(self.screen_height/2)), 15, (0,255, 0), -1)
            cv2.circle(self.board, (int(self.screen_width/2), int(self.screen_height/2)), 2, (0,0, 0), -1)
            #Draw left/ right limits
            r_limit_x = int((1-(self.r_limit)) * self.screen_width)
            l_limit_x = int((1-(self.l_limit)) * self.screen_width)
            cv2.line(self.board, (r_limit_x,int(self.screen_height/2-5)), (r_limit_x,int(self.screen_height/2+5)), (0,0,0),3)
            cv2.line(self.board, (l_limit_x,int(self.screen_height/2-5)), (l_limit_x,int(self.screen_height/2+5)), (0,0,0),3)

    ########################## Automatic Mode section ##################
    
    def init_auto(self):
        """
        Return:
            questions_counter_auto
            questions_number_auto : number of questions
        """
        questions_counter_auto = 0
        questions_number_auto = conf.max_iterations
        question_index = 0
        state_counter_auto = 0
        state_number_auto = conf.max_iterations
        state_index = 0
        choices = {1:None,2:None}
        choices_images = {1:None, 2:None}
        choices_map = {"left":1, "right":2}
        choice_index = None
        duration = 0
        questionAuto = None
        stateAuto = None
        notificationAuto = None
    
        return questions_counter_auto, questions_number_auto, question_index,state_counter_auto,state_number_auto,state_index ,choice_index, choices, choices_map, choices_images,\
        duration, questionAuto,stateAuto, notificationAuto

    def init_var_auto(self):
        self.camera_is_running = False

        self.questions_counter_auto, self.questions_number_auto, self.question_index, self.choice_index,\
        self.state_counter_auto, self.state_number_auto, self.state_index,\
        self.choices, self.choices_map,\
        self.choices_images, self.duration,\
        self.questionAuto,self.stateAuto, self.notificationAuto = self.init_auto()
        
        self.gaze_detected_previous = None
        self.start, self.end = None, None

        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
        
        self.notification_previous = False
        self.notification = None 

    def start_webcam_auto(self):
        if not self.camera_is_running:
            #Auto
            self.question_index = 0
            self.state_index = 0
            self.run_1()
            self.stateAuto, self.questionAuto, self.choices, self.choices_images, self.notificationAuto = self.run_explor_1(self.questions_counter_auto) # get the question and choices, set the images to None
            # print(self.stateAuto)
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            self.timer_auto = QtCore.QTimer(self)
            self.timer_auto.timeout.connect(self.update_frame_auto)
            self.timer_auto.start(2)


            # self.timer_auto.start(2)

    def stop_webcam_auto(self):
        if self.camera_is_running:
            self.capture.release()
            self.timer_auto.stop()
            self.camera_is_running = not self.camera_is_running
            #Showing model results
            self.run_experiments_2(self.questions_number_auto)
            #rectifying changes
            self.board = process.init_board(self.screen_height, self.screen_width)
            self.display_image(self.board, "stack_7")
   
    def mainButton_OnClick_auto(self):
        self.QtStack.setCurrentIndex(0)
        self.stop_webcam_auto()
    
    def init_var_auto_next(self):
        self.start, self.end = None, None
        self.duration = 0
        self.gaze_detected_previous = None
        self.question_index = 0
        self.state_index = 0

        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
    
    def next_auto_OnClick(self):
        if self.notification_previous and not self.camera_is_running:
            self.init_var_auto_next()
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            self.notification_previous = False
            self.notification = None     

    def notification_activated_auto(self):
        self.camera_is_running = False
        self.capture.release()

    ################## solution2 mode section ####################

    def init_auto_solution2(self):
        """
        Return:
            questions_counter_auto
            questions_number_auto : number of questions
        """
        questions_counter_sol2 = 0
        questions_number_sol2 = conf.max_iterations
        question_index_sol2 = 0
        state_counter_sol2 = 0
        state_number_sol2 = conf.max_iterations
        question_number_selection = conf.Number_of_actions
        questions_counter_selection = 0
        state_index_sol2 = 0
        choices = {1:None,2:None}
        choices_images = {1:None, 2:None}
        choices_map = {"left":1, "right":2}
        choice_index = None
        duration = 0
        questionSol2 = None
        stateSol2 = None
        notificationAuto = None
        return questions_counter_sol2, questions_number_sol2, question_index_sol2,state_counter_sol2,state_number_sol2,question_number_selection,questions_counter_selection,state_index_sol2 ,\
        choice_index, choices, choices_map, choices_images,\
        duration, questionSol2,stateSol2, notificationAuto

    def init_var_soltuion2(self):
        self.camera_is_running = False

        self.questions_counter_sol2, self.questions_number_sol2, self.question_index_sol2, self.choice_index,\
        self.state_counter_sol2, self.state_number_sol2, self.question_number_selection, self.questions_counter_selection,self.state_index_sol2,\
        self.choices, self.choices_map,\
        self.choices_images, self.duration,\
        self.questionSol2,self.stateSol2, self.notificationAuto = self.init_auto_solution2()
        
        self.gaze_detected_previous = None
        self.start, self.end = None, None

        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None
        
        self.notification_previous = False
        self.notification = None

    def notification_activated_solution2(self):
        self.camera_is_running = False
        self.capture.release()
    
    def init_var_solution2_next(self):
        self.start, self.end = None, None
        self.duration = 0
        self.gaze_detected_previous = None
        self.question_index_sol2 = 0
        self.state_index_sol2 = 0

        self.thresholds_left, self.thresholds_right = [], []
        self.threshold_left, self.threshold_right = None, None

    def next_soltuion2_OnClick(self):
        if self.notification_previous and not self.camera_is_running:
            self.init_var_solution2_next()
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            self.notification_previous = False
            self.notification = None 

    def start_webcam_solution2(self):
        if not self.camera_is_running:
            #Auto
            self.question_index_sol2 = 0
            self.state_index_sol2 = 0
            self.run_experiments_1()
            self.answer1="None"
            print(self.answer1)
            print(self.questions_counter_selection)
            self.stateSol2, self.questionSol2, self.choices, self.choices_images, self.notificationAuto = self.run_xcsr(self.questions_counter_sol2,self.questions_counter_selection,self.answer1)
            self.capture = cv2.VideoCapture(0)
            self.camera_is_running = True
            self.timer_sol2 = QtCore.QTimer(self)
            self.timer_sol2.timeout.connect(self.update_frame_solution2)
            self.timer_sol2.start(2)

    def stop_webcam_auto_solution2(self):
        if self.camera_is_running:
            self.capture.release()
            self.timer_sol2.stop()
            self.camera_is_running = not self.camera_is_running
            #Showing model results
            self.run_experiments_2(self.questions_number_sol2)
            #rectifying changes
            self.board = process.init_board(self.screen_height, self.screen_width)
            self.display_image(self.board, "stack_8")

##################################### solution2 ################################
    def update_frame_solution2(self):
        if self.notification_previous and not self.camera_is_running:
           pass
        elif not self.notification_previous and self.camera_is_running:
            _, frame = self.capture.read()
            self.frames_processed += 1
            if self.frames_processed % self.frames_to_process == 0:
                #self.frames_processed = 0
                #print(self.frames_processed)
                #choices window
                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                #getting questions , choices and images

                #drawing boxes
                for i in range(1,3):
                    letter_light = False
                    if (self.choice_index) and (i == self.choice_index):
                        letter_light = True
                    process.cell(i, self.choices[i], letter_light,self.board,self.choices_images[i])

                process.write_text("Situation :", cv2.FONT_HERSHEY_PLAIN, 2, 2, 200, 100, 0, 0, self.board)
                process.write_text(self.stateSol2, cv2.FONT_HERSHEY_PLAIN, 2, 2, 500, 200, 0, 0, self.board)
                process.write_text(self.questionSol2, cv2.FONT_HERSHEY_PLAIN, 2, 2, 400, 200, 500, 0, self.board)
                process.write_text("Nombre de tentative :", cv2.FONT_HERSHEY_PLAIN, 2, 2, 200, 100, 950, 0, self.board)
                process.write_text(str(self.questions_counter_selection+1), cv2.FONT_HERSHEY_PLAIN, 2, 2, 200, 200, 950, 0, self.board)
                
                # Converting the image to gray scale
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                # Get faces into webcam's image
                rects = self.detector(gray, 0)
                #variable used to detect the choice made by the patient
                self.choice_index = None
                #processing
                if rects:
                    for (i, rect) in enumerate(rects):
                        #drawing a rectangele
                        (x, y, w, h) = face_utils.rect_to_bb(rect)
                        face = frame[x-int(w/2):x+w,y:y+h+int(h/2)]
                        # Predict the landmarks ( points of the face)
                        landmarks = self.predictor(gray,rect)

                        #get the points defining the eye
                        eye_left, region_left, eye_left_center = process.detect_eye(self.LEFT_EYE_POINTS, landmarks, gray)
                        eye_right, region_right, eye_right_center = process.detect_eye(self.RIGHT_EYE_POINTS, landmarks, gray)
                        #detect the pupil
                        ##calibrating
                        if not process.is_complete(self.thresholds_left, self.thresholds_right):
                            process.calibrate(eye_left, self.thresholds_left)
                            process.calibrate(eye_right, self.thresholds_right)
                            return
                            print("pass")
                        if (not self.threshold_left) and (not self.threshold_right):
                            self.threshold_left = process.threshold(self.thresholds_left)
                            self.threshold_right = process.threshold(self.thresholds_right)
                        #print(self.threshold_left, self.threshold_right)
                        ##detect pupil
                        horizontal_ratio_left, eye_left_filtered = process.detect_pupil(eye_left_center, eye_left, self.threshold_left)
                        horizontal_ratio_right, eye_right_filtered = process.detect_pupil(eye_right_center, eye_right, self.threshold_right)
                        ##get the measurments of each eye
                        horizontal_ratio = process.mid_vals(horizontal_ratio_left, horizontal_ratio_right)
                        #gaze projection
                        ##get the measurments of each eye                
                        if (horizontal_ratio):
                            gaze_detected  = process.gaze_classifying_rl(horizontal_ratio, self.l_limit, self.r_limit)
                            if (gaze_detected):
                                self.choice_index = self.choices_map[gaze_detected]
                                if (self.gaze_detected_previous == None):
                                    self.start = time.time()
                                    self.gaze_detected_previous = gaze_detected
                                    self.notification_previous = False
                                elif (gaze_detected == self.gaze_detected_previous):
                                    self.end = time.time()
                                    self.duration = self.end - self.start
                                    cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
                                    if (self.duration > self.dwelling_time):
                                        self.gaze_detected_previous = None
                                        if gaze_detected == "left":
                                            #storing the answered question
                                            self.questions_answered.append([self.questionSol2,self.text_left,datetime.datetime.now().date(),datetime.datetime.now().time()])
                                            self.notification = self.notificationAuto[0]
                                            self.answer1="non"
                                            print(self.answer1)
                                            #EXPLOITES THE RESULTS
                                            # self.run_explor_2(self.questions_counter_selection,False)

                                            #GENERATES THE NEXT QUESTION
                                            self.questions_counter_selection+=1
                                            self.stateSol2, self.questionSol2, self.choices, self.choices_images, self.notificationAuto = self.run_xcsr(self.questions_counter_sol2,self.questions_counter_selection,self.answer1) # get the question and choices, set the images to None

                                            #change the questions counter
                                            # self.questions_counter_sol2 += 1
                                            # self.state_counter_sol2 +=1
                                            

                                            #display a notificaion
                                            if self.notification :
                                                self.notification_previous = True
                                                self.notification_activated_auto()
                                                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                                                process.write_text(self.notificationAuto, cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, 2, self.screen_width, self.screen_height, 0, 0, self.board)

                                            else:
                                                self.notification_previous = False

                                        elif gaze_detected == "right":
                                            #storing the answered question
                                            self.questions_answered.append([self.questionSol2,self.text_left,datetime.datetime.now().date(),datetime.datetime.now().time()])
                                            self.notification = self.notificationAuto[1]
                                            self.answer1="oui"
                                            print(self.answer1)
                                            #EXPLOITES THE RESULTS
                                            self.run_explor_2(self.questions_counter_sol2,True)


                                            #GENERATES THE NEXT QUESTION
                                            self.questions_counter_selection = 0
                                            self.questions_counter_sol2 += 1
                                            self.stateSol2, self.questionSol2, self.choices, self.choices_images, self.notificationAuto  = self.run_xcsr(self.questions_counter_sol2,self.questions_counter_selection,self.answer1) # get the question and choices, set the images to None
                                            #change the questions counter
                                            
                                            # self.state_counter_sol2+=1
                                            # self.question_index_sol2 = 0
                                            # self.state_index_sol2 = 0
                                            # self.questions_counter_selection+=1

                                            # if (self.questions_counter_selection == 8-1):
                                            #     self.questions_counter_selection = 0                                            
                                            #     self.questions_counter_sol2 += 1
                                            #     self.question_index_sol2 = 0
                                            #     self.state_index_sol2 = 0
                                            # if(self.questions_counter_sol2== questions_number_auto-1):
                                            #     self.stop_webcam_auto_solution2()
                                            #     break
                                            #     self.question_index_sol2 = 0
                                            #     self.state_index_sol2 = 0

                                            #display a notificaion
                                            if self.notification :
                                                self.notification_previous = True
                                                self.notification_activated_auto()
                                                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                                                process.write_text(self.notification, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5, 2, self.screen_width, self.screen_height, 0, 0, self.board)

                                            else:
                                                self.notification_previous = False
                                    #prevent showing time in the notification screen
                                    if not self.notification_previous :
                                        cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)      
                                elif (gaze_detected != self.gaze_detected_previous):                 
                                    self.start = time.time()
                                    self.gaze_detected_previous = gaze_detected
                                    self.notification_previous = False
                            elif not gaze_detected:
                                self.gaze_detected_previous = None
                                self.notification_previous = False

                        for (x, y) in np.concatenate((region_left,region_right)):
                            cv2.circle(frame, (x, y), 2, (0, 255, 0), -1)

                    #self.projection(horizontal_ratio)
                    #cv2.imshow("left_eye", eye_left_filtered)
                    #cv2.imshow("frame",frame)
     
                else:
                    print(rects)

        self.display_image(self.board, "stack_8")#edit stack for other uses

################################ UPDATE FRAME AUTO ##################################
    def update_frame_auto(self):
        if self.notification_previous and not self.camera_is_running:
           pass
        elif not self.notification_previous and self.camera_is_running:
            _, frame = self.capture.read()
            self.frames_processed += 1
            if self.frames_processed % self.frames_to_process == 0:
                #self.frames_processed = 0
                #print(self.frames_processed)
                #choices window
                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                #getting questions , choices and images

                #drawing boxes
                for i in range(1,3):
                    letter_light = False
                    if (self.choice_index) and (i == self.choice_index):
                        letter_light = True
                    process.cell(i, self.choices[i], letter_light,self.board,self.choices_images[i])

                process.write_text("Situation :", cv2.FONT_HERSHEY_PLAIN, 2, 2, 200, 100, 0, 0, self.board)
                process.write_text(self.stateAuto, cv2.FONT_HERSHEY_PLAIN, 2, 2, 500, 200, 0, 0, self.board)
                process.write_text(self.questionAuto, cv2.FONT_HERSHEY_PLAIN, 2, 2, 400, 200, 500, 0, self.board)
                
                # Converting the image to gray scale
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                # Get faces into webcam's image
                rects = self.detector(gray, 0)
                #variable used to detect the choice made by the patient
                self.choice_index = None
                #processing
                if rects:
                    for (i, rect) in enumerate(rects):
                        #drawing a rectangele
                        (x, y, w, h) = face_utils.rect_to_bb(rect)
                        face = frame[x-int(w/2):x+w,y:y+h+int(h/2)]
                        # Predict the landmarks ( points of the face)
                        landmarks = self.predictor(gray,rect)

                        #get the points defining the eye
                        eye_left, region_left, eye_left_center = process.detect_eye(self.LEFT_EYE_POINTS, landmarks, gray)
                        eye_right, region_right, eye_right_center = process.detect_eye(self.RIGHT_EYE_POINTS, landmarks, gray)
                        #detect the pupil
                        ##calibrating
                        if not process.is_complete(self.thresholds_left, self.thresholds_right):
                            process.calibrate(eye_left, self.thresholds_left)
                            process.calibrate(eye_right, self.thresholds_right)
                            return
                            print("pass")
                        if (not self.threshold_left) and (not self.threshold_right):
                            self.threshold_left = process.threshold(self.thresholds_left)
                            self.threshold_right = process.threshold(self.thresholds_right)
                        #print(self.threshold_left, self.threshold_right)
                        ##detect pupil
                        horizontal_ratio_left, eye_left_filtered = process.detect_pupil(eye_left_center, eye_left, self.threshold_left)
                        horizontal_ratio_right, eye_right_filtered = process.detect_pupil(eye_right_center, eye_right, self.threshold_right)
                        ##get the measurments of each eye
                        horizontal_ratio = process.mid_vals(horizontal_ratio_left, horizontal_ratio_right)
                        #gaze projection
                        ##get the measurments of each eye                
                        if (horizontal_ratio):
                            gaze_detected  = process.gaze_classifying_rl(horizontal_ratio, self.l_limit, self.r_limit)
                            if (gaze_detected):
                                self.choice_index = self.choices_map[gaze_detected]
                                if (self.gaze_detected_previous == None):
                                    self.start = time.time()
                                    self.gaze_detected_previous = gaze_detected
                                    self.notification_previous = False
                                elif (gaze_detected == self.gaze_detected_previous):
                                    self.end = time.time()
                                    self.duration = self.end - self.start
                                    cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
                                    if (self.duration > self.dwelling_time):
                                        self.gaze_detected_previous = None
                                        if gaze_detected == "left":
                                            #storing the answered question
                                            self.questions_answered.append([self.questionAuto,self.text_left,datetime.datetime.now().date(),datetime.datetime.now().time()])
                                            self.notification = self.notificationAuto[0]

                                            #EXPLOITES THE RESULTS
                                            self.run_explor_2(self.questions_counter_auto,False)

                                            #change the questions counter
                                            self.questions_counter_auto += 1
                                            self.state_counter_auto +=1

                                            #GENERATES THE NEXT QUESTION
                                            self.stateAuto, self.questionAuto, self.choices, self.choices_images, self.notificationAuto = self.run_explor_1(self.questions_counter_auto) # get the question and choices, set the images to None


                                            if (self.questions_counter_auto == self.questions_number_auto-1):
                                                self.stop_webcam_auto()
                                                break
                                                self.question_index = 0
                                                self.state_index = 0

                                            #display a notificaion
                                            if self.notification :
                                                self.notification_previous = True
                                                self.notification_activated_auto()
                                                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                                                process.write_text(self.notificationAuto, cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, 2, self.screen_width, self.screen_height, 0, 0, self.board)

                                            else:
                                                self.notification_previous = False
                                        elif gaze_detected == "right":
                                            #storing the answered question
                                            self.questions_answered.append([self.questionAuto,self.text_left,datetime.datetime.now().date(),datetime.datetime.now().time()])
                                            self.notification = self.notificationAuto[1]

                                            #EXPLOITES THE RESULTS
                                            self.run_explor_2(self.questions_counter_auto,True)

                                            #change the questions counter
                                            self.questions_counter_auto += 1
                                            self.state_counter_auto +=1

                                            #GENERATES THE NEXT QUESTION
                                            self.stateAuto, self.questionAuto, self.choices, self.choices_images, self.notificationAuto  = self.run_explor_1(self.questions_counter_auto) # get the question and choices, set the images to None
                                            
                                            if (self.questions_counter_auto == self.questions_number_auto-1):
                                                self.stop_webcam_auto()
                                                break
                                                self.question_index = 0

                                            #display a notificaion
                                            if self.notification :
                                                self.notification_previous = True
                                                self.notification_activated_auto()
                                                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                                                process.write_text(self.notification, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5, 2, self.screen_width, self.screen_height, 0, 0, self.board)

                                            else:
                                                self.notification_previous = False
                                    #prevent showing time in the notification screen
                                    if not self.notification_previous :
                                        cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)      
                                elif (gaze_detected != self.gaze_detected_previous):                 
                                    self.start = time.time()
                                    self.gaze_detected_previous = gaze_detected
                                    self.notification_previous = False
                            elif not gaze_detected:
                                self.gaze_detected_previous = None
                                self.notification_previous = False

                        for (x, y) in np.concatenate((region_left,region_right)):
                            cv2.circle(frame, (x, y), 2, (0, 255, 0), -1)

                    #self.projection(horizontal_ratio)
                    #cv2.imshow("left_eye", eye_left_filtered)
                    #cv2.imshow("frame",frame)
     
                else:
                    print(rects)

        self.display_image(self.board, "stack_7")#edit stack for other uses

############################ update_frame_ask ########################
    def update_frame_ask(self):
        # Get a new frame from the webcam
        _, frame = self.capture.read()

        # Converting the captured frame to gray scale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        # Detect the faces 
        rects = self.detector(gray, 0)

        #Variable used to detect the choice made by the patient
        choice_index = None
        
        #Initialize the board
        self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)

        #Show the question
        if self.question_show_activated == True:
            time.sleep(self.question_time)
            self.question_show_activated = False

        if rects: #if there a detection of a fexe
            for (i, rect) in enumerate(rects):
                # Crop the face from the frame
                (x, y, w, h) = face_utils.rect_to_bb(rect)
                face = frame[x-int(w/2):x+w,y:y+h+int(h/2)]

                # Predict the facial landmarks (points of the face)
                landmarks = self.predictor(gray,rect)

                # Detect the eyes
                eye_left, region_left, eye_left_center = process.detect_eye(self.LEFT_EYE_POINTS, landmarks, gray)
                eye_right, region_right, eye_right_center = process.detect_eye(self.RIGHT_EYE_POINTS, landmarks, gray)

                # Calibrate the threshold
                if not process.is_complete(self.thresholds_left, self.thresholds_right):
                    process.calibrate(eye_left, self.thresholds_left)
                    process.calibrate(eye_right, self.thresholds_right)
                    pass
                self.threshold_left = process.threshold(self.thresholds_left)
                self.threshold_right = process.threshold(self.thresholds_right)

                # Detect the pupil
                horizontal_ratio_left, eye_left_filtered=process.detect_pupil(eye_left_center, eye_left, self.threshold_left)
                horizontal_ratio_right, eye_right_filtered=process.detect_pupil(eye_right_center, eye_right, self.threshold_right)

                #Get the position of the pupil within the eye (as a ratio = pupil_x_position/eye_width)
                horizontal_ratio = process.mid_vals(horizontal_ratio_left, horizontal_ratio_right)
                if (horizontal_ratio):
                    #Detect the direction of the gaze : left or right
                    gaze_detected  = process.gaze_classifying_rl(horizontal_ratio, self.l_limit, self.r_limit)
                    if (gaze_detected):
                        choice_index = self.choices_map[gaze_detected]
                        if (self.gaze_detected_previous == None):
                            self.start = time.time()
                            self.gaze_detected_previous = gaze_detected
                        elif (gaze_detected == self.gaze_detected_previous):
                            self.end = time.time()
                            self.duration = self.end - self.start
                            cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
                            if (self.duration > self.dwelling_time):
                                self.gaze_detected_previous = None
                                if gaze_detected == "left":
                                    self.choice_selected = self.choices[1]
                                if gaze_detected == "right":
                                    self.choice_selected = self.choices[2]
                                self.stop_webcam_ask()
                                break
                        elif (gaze_detected != self.gaze_detected_previous):                 
                            self.start = time.time()
                            self.gaze_detected_previous = gaze_detected
                    elif not gaze_detected:
                        self.gaze_detected_previous = None
            process.write_text(self.question, cv2.FONT_HERSHEY_COMPLEX_SMALL, 3, 4, self.screen_width, int(self.screen_height/2), 0, 0, self.board)
            if self.camera_is_running:
                for i in range(1,3):
                    letter_light = False
                    if (choice_index) and (i == choice_index):
                        letter_light = True
                    process.cell_ask(i, self.choices[i], letter_light,self.board)

        self.display_image(self.board, "stack_5")#edit stack for other uses

#################################### update_frame guided ############################################
    def update_frame(self):
        """
        """
        if self.notification_previous and not self.camera_is_running:
           pass
        elif not self.notification_previous and self.camera_is_running:
            # Get a new frame from the webcam
            _, frame = self.capture.read()
            #Increment the numbre of frames captured
            self.frames_processed += 1
            if self.frames_processed % self.frames_to_process == 0:
                #self.frames_processed = 0
                #print(self.frames_processed)

                #Initialize the board
                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                
                #Draw the choices boxes
                for i in range(1,3):
                    letter_light = False
                    if (self.choice_index) and (i == self.choice_index):
                        letter_light = True
                    process.cell(i, self.choices[i], letter_light,self.board,self.choices_images[i])
                #Shiw the question in teh board
                if self.question_current != "debut":
                    process.write_text(self.question_current, cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, 2, self.screen_width, int(self.screen_height/3), 0, 0, self.board)
                
                # Converting the captured frame to gray scale
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # Detect the faces 
                rects = self.detector(gray, 0)

                #Variable used to detect the choice made by the patient
                self.choice_index = None
                
                #Processing
                if rects:
                    for (i, rect) in enumerate(rects):
                        # Crop the face from the frame
                        (x, y, w, h) = face_utils.rect_to_bb(rect)
                        face = frame[x-int(w/2):x+w,y:y+h+int(h/2)]
                        
                        # Predict the facial landmarks (points of the face)
                        landmarks = self.predictor(gray,rect)

                        # Detect the eyes
                        eye_left, region_left, eye_left_center = process.detect_eye(self.LEFT_EYE_POINTS, landmarks, gray)
                        eye_right, region_right, eye_right_center = process.detect_eye(self.RIGHT_EYE_POINTS, landmarks, gray)
                        
                        # Calibrate the threshold
                        if not process.is_complete(self.thresholds_left, self.thresholds_right):
                            process.calibrate(eye_left, self.thresholds_left)
                            process.calibrate(eye_right, self.thresholds_right)
                            return
                            print("pass")
                        if (not self.threshold_left) and (not self.threshold_right):
                            self.threshold_left = process.threshold(self.thresholds_left)
                            self.threshold_right = process.threshold(self.thresholds_right)

                        #print(self.threshold_left, self.threshold_right)

                        # Detect the pupil
                        horizontal_ratio_left, eye_left_filtered = process.detect_pupil(eye_left_center, eye_left, self.threshold_left)
                        horizontal_ratio_right, eye_right_filtered = process.detect_pupil(eye_right_center, eye_right, self.threshold_right)
                        
                        #Get the position of the pupil within the eye (as a ratio = pupil_x_position/eye_width)
                        horizontal_ratio = process.mid_vals(horizontal_ratio_left, horizontal_ratio_right)

                        if (horizontal_ratio):
                            """
                            Detect the gaze and the choice:
                                *case 1 : if there is no gaze detected in the previous frame
                                    -Start the time measuring 
                                *case 2 : if the gaze detected in the previous frame is the same as in the actual frame
                                    -Stop the time measuring and calculate the duration of gazing
                                    -Check if the duration of gazinf is greater than the dwelling time, if True:
                                        *Store the answer
                                        *Update the question and the choices
                                        *Dsiplay the notification if it exists
                                *case 3 : if the gaze detected in the previous frame isn't the same as in the actual frame
                                    -Start the time measuring


                            """
                            #Detect the direction of the gaze : left or right
                            gaze_detected  = process.gaze_classifying_rl(horizontal_ratio, self.l_limit, self.r_limit)
                            if (gaze_detected):
                                self.choice_index = self.choices_map[gaze_detected]
                                #Case 1
                                if (self.gaze_detected_previous == None):
                                    self.start = time.time()
                                    self.gaze_detected_previous = gaze_detected
                                    self.notification_previous = False
                                #Case 2
                                elif (gaze_detected == self.gaze_detected_previous):
                                    self.end = time.time()
                                    self.duration = self.end - self.start
                                    cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1)
                                    if (self.duration > self.dwelling_time):
                                        self.gaze_detected_previous = None
                                        if gaze_detected == "left":
                                            #Store the answered question
                                            self.questions_answered.append([self.question_current,self.text_left,datetime.datetime.now().date(),datetime.datetime.now().time()])
                                            self.notification = self.questions_choices_notifications[self.question_index][0]
                                            index_jump = self.questions_next[self.question_index][0]
                                            #Change the current index
                                            self.question_index = self.question_index + index_jump
                                            if (self.question_index == self.questions_number - 1):
                                                self.stop_webcam_guided()
                                                break
                                                self.question_index = 0
                                            #Updating question, choices and images
                                            self.question_current = self.questions_names[self.question_index]
                                            self.text_left = self.questions_choices[self.question_index][0]
                                            self.text_right = self.questions_choices[self.question_index][1]
                                            self.image_left = cv2.imread(self.questions_images[self.question_index][0])
                                            self.image_right = cv2.imread(self.questions_images[self.question_index][1])
                                            self.choices[1] = self.text_left
                                            self.choices[2] = self.text_right
                                            self.choices_images[1] = self.image_left
                                            self.choices_images[2] = self.image_right
                                            #Display a notificaion
                                            if self.notification :
                                                self.notification_previous = True
                                                self.notification_activated()
                                                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                                                process.write_text(self.notification, cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, 2, self.screen_width, self.screen_height, 0, 0, self.board)

                                            else:
                                                self.notification_previous = False
                                        elif gaze_detected == "right":
                                            #Store the answered question
                                            self.questions_answered.append([self.question_current,self.text_right,datetime.datetime.now().date(),datetime.datetime.now().time()])
                                            self.notification = self.questions_choices_notifications[self.question_index][1]
                                            index_jump = self.questions_next[self.question_index][1]
                                            #Change the current index
                                            self.question_index = self.question_index + index_jump
                                            if (self.question_index == self.questions_number - 1):
                                                self.stop_webcam_guided()
                                                break
                                                self.question_index = 0
                                            #Updating question, choices and images
                                            self.question_current = self.questions_names[self.question_index]
                                            self.text_left = self.questions_choices[self.question_index][0]
                                            self.text_right = self.questions_choices[self.question_index][1]
                                            self.image_left = cv2.imread(self.questions_images[self.question_index][0])
                                            self.image_right = cv2.imread(self.questions_images[self.question_index][1])
                                            self.choices[1] = self.text_left
                                            self.choices[2] = self.text_right
                                            self.choices_images[1] = self.image_left
                                            self.choices_images[2] = self.image_right
                                            #Display a notificaion
                                            if self.notification :
                                                self.notification_previous = True
                                                self.notification_activated()
                                                self.board = np.full((self.screen_height, self.screen_width,3), 255, np.uint8)
                                                process.write_text(self.notification, cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, 2, self.screen_width, self.screen_height, 0, 0, self.board)

                                            else:
                                                self.notification_previous = False
                                    #Show the duration
                                    if not self.notification_previous :
                                        cv2.putText(self.board, "{:03.2f}".format(self.duration),(548,200),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,0),1) 
                                #Case 3     
                                elif (gaze_detected != self.gaze_detected_previous):                 
                                    self.start = time.time()
                                    self.gaze_detected_previous = gaze_detected
                                    self.notification_previous = False

                            elif not gaze_detected:
                                self.gaze_detected_previous = None
                                self.notification_previous = False
                        #Draw the eye landmarks
                        for (x, y) in np.concatenate((region_left,region_right)):
                            cv2.circle(frame, (x, y), 2, (0, 255, 0), -1)

                    #self.projection(horizontal_ratio)
                    #Show the left eye
                    cv2.imshow("left_eye", eye_left_filtered)
                    #Show the frame
                    cv2.imshow("frame",frame)
     
                else: #No face is detected
                    print(rects)
        #Display the board
        self.display_image(self.board, "stack_3")#edit stack for other uses

if __name__ == '__main__':
    app = QApplication(sys.argv)
    showMain = MainWindow()
    sys.exit(app.exec_())
