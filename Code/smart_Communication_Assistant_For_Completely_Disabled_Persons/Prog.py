import numpy as np
import matplotlib.pyplot as plt
import random
import csv
import os.path
from Config import *
from Environment import *
from Classifier import *
from ClassifierSet import *
from MatchSet import *
from random import randint
import Config
from ActionSet import *
import numpy
import numpy.random
from copy import deepcopy
import pandas as pd


os.chdir("C:/Users/RAHMA/Desktop/smart_Communication_Assistant_For_Completely_Disabled_Persons")


textdata1 = pd.read_csv('8actions_DS.csv')
real_action = textdata1['real_action'].values.tolist()
size_simulation=len(real_action)




class Prog:
    def __init__(self):
        self.env = Environment()
    def init(self,actual_time):
        self.env = Environment()
        self.PCC=0

    def init2(self,actual_time):
        
        self.env = Environment()
        self.PCC = 0
        textdata = pd.read_csv('population10000.csv')
        
        tmin = textdata['tmin'].values.tolist()
        tmax = textdata['tmax'].values.tolist()
        
        tpmin = textdata['tpmin'].values.tolist()
        tpmax = textdata['tpmax'].values.tolist()
        
        tp_w_min= textdata['tp_weather_min'].values.tolist()
        tp_w_max= textdata['tp_weather_max'].values.tolist()
        
        lmin = textdata['lmin'].values.tolist()
        lmax = textdata['lmax'].values.tolist()
        
        state_window_min = textdata['state_window_min'].values.tolist()
        state_window_max = textdata['state_window_max'].values.tolist()
        
        state_bed_min = textdata['state_bed_min'].values.tolist()
        state_bed_max = textdata['state_bed_max'].values.tolist()
        
        action = textdata['action'].values.tolist()
        fit = textdata['fitness'].values.tolist()
        predict = textdata['prediction'].values.tolist()
        err = textdata['error'].values.tolist()
        num = textdata['numerosity'].values.tolist()
        exper = textdata['experience'].values.tolist()
        timestamp = textdata['time_stamp'].values.tolist()
        actionsetsize = textdata['action_set_size'].values.tolist()     

        i=0
        while len(self.pop.clSet) < len(tmin):
            cond=[tmin[i],tmax[i],tpmin[i],tpmax[i],tp_w_min[i],tp_w_max[i],lmin[i],lmax[i],state_window_min[i],state_window_max[i],state_bed_min[i],state_bed_max[i]]
            clm = Classifier(cond,actual_time)
            clm.condition = cond
            clm.action = action[i]
            clm.time_stamp = timestamp[i]
            clm.numerosity = num[i]
            clm.action_set_size = actionsetsize[i]
            clm.prediction = predict[i]
            clm.error = err[i]
            clm.fitness = fit[i]
            clm.experience = exper[i]
            self.pop.clSet.append(clm)
            i+=1


    def run_1(self):
        self.actual_time = 0.0
        self.pop = ClassifierSet(self.env,self.actual_time)
        self.init2(self.actual_time)


    def run_experiments_1(self):
        self.actual_time = 0.0
        self.pop = ClassifierSet(self.env,self.actual_time)
        self.init(self.actual_time)

    def run_experiments_2(self,questions_number_auto):
        self.file_writer(questions_number_auto)
        PCC_total=self.PCC/(questions_number_auto)
        print("PCC 1    "+ str(self.PCC) + "/" + str(questions_number_auto))
        print("PCC :   " + str(PCC_total) + " %")

    def run_explor_1(self,i):

        self.env.set_state(i)
        stateAuto = str(self.env.state[0])+"/"+str(self.env.state[1])+"/"+str(self.env.state[2])+"/"+str(self.env.state[3])+"/"+str(self.env.state[4])+"/"+str(self.env.state[5])

        print(stateAuto)

        self.match_set = MatchSet(self.pop,self.env,self.actual_time)

        self.generate_prediction_array()

        questionAuto, choices, notificationAuto  = self.select_action_0()
        
        self.action_set = ActionSet(self.match_set,self.action,self.env,self.actual_time)

        return  stateAuto, questionAuto, choices, {1:None,2:None}, [None,notificationAuto]

    

    def run_xcsr(self,i,j,answer1):

        self.env.set_state(i)
        
        stateAuto = str(self.env.state[0])+"/"+str(self.env.state[1])+"/"+str(self.env.state[2])+"/"+str(self.env.state[3])+"/"+str(self.env.state[4])+"/"+str(self.env.state[5])

        print(stateAuto)

        self.match_set = MatchSet(self.pop,self.env,self.actual_time)

        self.generate_prediction_array()

        if answer1=="None":
            self.list_rank = self.get_list_with_rank(8)

        if answer1 == "oui":
            print(self.p_array)
            self.list_rank = self.get_list_with_rank(8)
            # self.list_rank = [0,2,1,3]
            self.list_no_change = self.list_rank
        else:
            self.list_no_change = self.list_rank

        print(self.list_no_change)

        questionAuto, choices, notificationAuto = self.select_action(j)
        
        self.action_set = ActionSet(self.match_set,self.action,self.env,self.actual_time)

        return  stateAuto, questionAuto, choices, {1:None,2:None}, [None,notificationAuto]
        
    def run_explor_2(self,i, answer):        

        self.action_set.do_action_0(i,answer)

        if self.action_set.get_do_action() == 1000:
            self.PCC+=1

        self.action_set.update_action_set()

        self.action_set.do_action_set_subsumption(self.pop)

        self.run_GA()

        if len(self.pop.clSet) > conf.Max_size_population:
            self.pop.delete_from_population()
        self.actual_time += 1.0

    def select_action_0(self):
        if random.random() > conf.p_explr:
            self.action = self.best_action()
        else:
            self.action = random.randrange(Config.Actions_count)
        questionAuto = str(Config.actions_list[self.action])
        # print(questionAuto)
        notificationAuto = str(Config.notification_list[self.action])
        choices = {1:"No", 2:"Yes"}
        return questionAuto, choices,notificationAuto

    def select_action(self,j):
        self.action = self.list_no_change[j]
        print(self.action)
        questionAuto = str(Config.actions_list[self.action])
        print(questionAuto)
        notificationAuto = str(Config.notification_list[self.action])
        choices = {1:"No", 2:"Yes"}
        return questionAuto, choices,notificationAuto
        
#        print(self.action)

    def get_list_with_rank(self,rank):
        #trier 
        l = sorted( [(x,i) for (i,x) in enumerate(self.p_array)], reverse=True )[:rank]
        self.values = []
        self.posns = []
        for x,i in l:
            self.values.append( x )
            self.posns.append( i )
        return self.posns

    def get_list_actions(self):
        list_actions = []
        for i in range(conf.Number_of_actions):
            list_actions.append(i)
        return list_actions

    def get_rest_list_action(self,list_top):
        #return to the list "list_reste" actions that have not been proposed
        
        list_actions=self.get_list_actions()
        list_reste = []
        for i in range(len(list_actions)):
            if list_actions[i] not in list_top:
                list_reste.append(list_actions[i])
        return list_reste


    def best_action(self):
        big = self.p_array[0]
        best = 0
        for i in range(Config.Actions_count):
            if big < self.p_array[i]:
                big = self.p_array[i]
                best = i
        return best

    """
        Generates a prediction array for the given match set
        @param match_set - The match set to generate predictions for
    """
    def generate_prediction_array(self):
        self.p_array = [0] * conf.Number_of_actions
        self.f_array = [0] * conf.Number_of_actions
        for cl in self.match_set.clSet:
            self.p_array[cl.action] += cl.prediction*cl.fitness
            self.f_array[cl.action] += cl.fitness
        for i in range(conf.Number_of_actions):
            if self.f_array[i] != 0:
                self.p_array[i] /= self.f_array[i]


    def select_offspring(self):
        fit_sum=0
        for q in self.action_set.clSet:
            fit_sum = fit_sum + q.fitness
        choice_point = fit_sum * random.random()
        fit_sum = 0.0
        for cl in self.action_set.clSet:
            fit_sum = fit_sum + cl.fitness
            if fit_sum > choice_point:
                return cl
        return None

    def twoPointCrossover(self,cl1,cl2):

        length = len(cl1.condition)
        sep1 = int(random.random()*(length))
        sep2 = int(random.random()*(length))
        if sep1>sep2:
            sep1,sep2 = sep2,sep1
        elif sep1==sep2:
            sep2 = sep2+1
        cond1 = cl1.condition
        cond2 = cl2.condition

        for i in range(sep1,sep2):
            if cond1[i] != cond2[i]:
                cond1[i],cond2[i] = cond2[i],cond1[i]
        cl1.condition = cond1
        cl2.condition = cond2

    def add_Time(self,time,time_plus):
        time=str(time)
        minute_part = time[3] + time[4]
        hour_part = time[0] + time[1]
        minute = int(minute_part) + time_plus
        if minute >= 60:
            minute = minute-60
            hour = int(hour_part)+1
        else:
            hour = int(hour_part)
        if hour < 10:
            hour = "0" + str(hour)
        if minute < 10:
            minute = "0" + str(minute)
        Tmax = str(hour) + ":" + str(minute)
        return (Tmax)

    def add_Number(self,val,rand_num):
        new_val= val + rand_num
        return new_val

    def add_Random_Value(self,attr1,attr2):
        if isinstance(attr1,str)==True and isinstance(attr2, str)==True:
            #ajouter une valeur aleatoire pour le temps d'une val random de 15 minutes
            rand_temps=randint(1,15)
            at1 = self.add_Time(attr1,rand_temps)
            at2 = self.add_Time(attr2,rand_temps)
        else:
            #ajouter pour les autres variables valeur = +/- 4
            rand_number=randint(1,4)
            at1 = self.add_Number(attr1,rand_number)
            at2 = self.add_Number(attr2,rand_number)
        return at1,at2

    def apply_mutation(self,cl):
        if random.random() < conf.proba_mutation:
            #mutation pour la condition
            if len(cl.condition) % 2 == 0:
                number_interval = (len(cl.condition)/2)-1
                rand_interval = randint(0,number_interval)
                indice_borne_inf = rand_interval*2
                indice_borne_sup = (rand_interval*2) + 1
                e1, e2 = self.add_Random_Value(cl.condition[indice_borne_inf],cl.condition[indice_borne_sup])
                cl.condition[indice_borne_inf] = e1
                cl.condition[indice_borne_sup] = e2

        if random.random() < conf.proba_mutation:
            #mutation pour les action
            cl.action = random.randrange(conf.Number_of_actions)


    def run_GA(self):
        if len(self.action_set.clSet)==0:
            return
        if self.actual_time - self.action_set.ts_num_sum()/self.action_set.numerosity_sum()>conf.theta_ga:
            for cl in self.action_set.clSet:
                cl.time_stamp = self.actual_time

    def file_writer(self,num):
        file_name = "population"+str(num)+".csv"
        write_csv = csv.writer(open(file_name,'w'),lineterminator='\n')
        write_csv.writerow(["tmin","tmax","tpmin","tpmax","tp_weather_min","tp_weather_max","lmin","lmax","state_window_min","state_window_max","state_bed_min","state_bed_max","action","fitness","prediction","error","numerosity","experience","time_stamp","action_set_size"])
        for cl in self.pop.clSet:

            cond = ""
            for c in cl.condition:
                cond+=str(c)+"-"
                tmin=cl.condition[0]
                tmax=cl.condition[1]
                tpmin=cl.condition[2]
                tpmax=cl.condition[3]
                tp_weather_min=cl.condition[4]
                tp_weather_max=cl.condition[5]
                lmin=cl.condition[6]
                lmax=cl.condition[7]
                state_window_min=cl.condition[8]
                state_window_max=cl.condition[9]
                state_bed_min=cl.condition[10]
                state_bed_max=cl.condition[11]
            write_csv.writerow([tmin,tmax,tpmin,tpmax,tp_weather_min,tp_weather_max,lmin,lmax,state_window_min,state_window_max,state_bed_min,state_bed_max,cl.action,cl.fitness,cl.prediction,cl.error,cl.numerosity,cl.experience,cl.time_stamp,cl.action_set_size])




# if __name__ == '__main__':
#     xcs=Prog()
#     print("data set size :   " +str(size_simulation))
#     print("Iteration number :   " +str(conf.max_iterations))
#     print("Max size population :   " +str(conf.Max_size_population))
#     xcs.run_experiments()
