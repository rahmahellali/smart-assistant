import random
from Config import *
from Environment import *
from Classifier import *
from ClassifierSet import *
from MatchSet import *
from random import randint

class ActionSet(ClassifierSet):
    def __init__(self,match_set,action,state,actual_time):
        ClassifierSet.__init__(self,state,actual_time)
        self.action = action
        for cl in match_set.clSet:
            if cl.action == self.action:
                self.clSet.append(cl)
    def do_action(self,i): #simulation
        if self.env.is_true(self.action,i):
            self.reward = 1000
        else:
            self.reward = 0

    def do_action_0(self,i,answer): #simulation
        if self.env.is_true_0(self.action,i, answer):
            self.reward = 1000
        else:
            self.reward = 0

    def get_do_action(self):
        return self.reward

    """
       Updates the given action set's prediction, error, average size and fitness using the given decayed performance
       @param reward - The reward to use
    """
    def update_action_set(self):
        num_sum = self.numerosity_sum()
        for cl in self.clSet:
            cl.update_parameters(self.reward,num_sum)
        acc_sum = self.accuracy_sum()
        for cl in self.clSet:
            cl.update_fitness(acc_sum)
    """
        Does subsumption inside the action set, finding the most general classifier
        and merging things into it
    """
    def do_action_set_subsumption(self,pop):
        if conf.doActionSetSubsumtion:
            subsumer = None
            for cl in self.clSet:
                if cl.could_subsume():
                    if subsumer==None or cl.is_more_general(subsumer):
                        subsumer = cl
            if subsumer!=None:
                i=0
                while i<len(self.clSet):
                    if subsumer.is_more_general(self.clSet[i]):
                        subsumer.numerosity += self.clSet[i].numerosity
                        pop.remove_classifier_by_instance(self.clSet[i])
                        self.remove_classifier(i)
                        i -= 1
                    i += 1

