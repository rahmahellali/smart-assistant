#this code defines the processing functions for gaze tracking
from imutils import face_utils
import dlib
import cv2
import numpy as np
from time import time


from win32api import GetSystemMetrics

#Variables of questions' scenario
##The list of questions
questions_names = [
"debut",\
"Vous etes en urgence ?",\
"ouvrir la fenetre",\
"changer position",\
"changer position vers bas",\
"changer position vers haut",\
"faire la toilette",\
"prendre petit djeuneer",\
"deplacer",\
"appeler quelqu'un",\
"Que voulez vous appeler",\
"divertissement",\
"Que voulez vous faire",\
"Que voulez vous ecoutez ?",\
"Que voulez vous regardez ?",\
"fin",\
]

#Choices for each question
questions_choices = [
["urgence", "activite_qutidienne"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["Non", "Oui"],\
["responsable", "ami"],\
["Non", "Oui"],\
["ecouter", "TV"],\
["musique", "coran"],\
["Mecca", "TN"],\
[None, None],\
]
#Notifications after each choice
questions_choices_notifications = [
[None, None],\
[None, "le service d'urgence est appele"],\
[None, "un infirmiere viendra dans 2 mins"],\
[None, None],\
[None, "position change vers bas"],\
[None, "position change vers haut"],\
[None, "un infirmiere viendra dans 2 mins"],\
[None, "un infirmiere viendra dans 2 mins"],\
[None, "un infirmiere viendra dans 2 mins"],\
[None, None],\
["appel en cours", "appel en cours"],\
[None, None],\
[None, None],\
["un infirmiere viendra dans 2 mins", "un infirmiere viendra dans 2 mins"],\
["un infirmiere viendra dans 2 mins", "un infirmiere viendra dans 2 mins"],\
[None, None],\
]
#The list of jumps in the list of questions relatively to the current question after each choice
""" i.e : 2 means that after choosing, the next question will the one having the index = current_index + 2 """
questions_number = len(questions_names)

questions_next = [
[1, 2],\
[questions_number - 2, 1],\
[1, 1],\
[3, 1],\
[1, -1],\
[1, -2],\
[1, 1],\
[1, 1],\
[1, 1],\
[2, 1],\
[1, 1],\
[3, 1],\
[1, 2],\
[2, 2],\
[1, 1],\
[None, None],\
]

#The list of images for each choice
##Loading images
image_person_1 = "person_1.jpg"
image_person_2 = "person_2.jpg"
## The list
questions_images = [
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[image_person_1, image_person_2],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
[None, None],\
]

def get_screen_size():
	"""
	Get screen dimensions in order to set GUI dimensions relatively to the screen
	
	Code for windows:
	from win32api import GetSystemMetrics
	x_dim, y_dim=GetSystemMetrics(0), GetSystemMetrics(1)
	
	Cide for windows and linux :
	from pymouse import PyMouse
	mouse = PyMouse()
	x_dim, y_dim = mouse.screen_size()
	"""
	x_dim, y_dim=GetSystemMetrics(0), GetSystemMetrics(1)
	marge_screen = 50
	screen_width, screen_height = x_dim , y_dim - marge_screen
	return screen_width, screen_height
screen_width, screen_height = get_screen_size()

# Functions for initializing some variables : 
def init_cv():
	"""Return face, landmarks and eye detectors"""

	#Face detector is used to detect faces
	detector = dlib.get_frontal_face_detector()
	#Landmarks predictor is used to get facial landmarks of a given face
	p = "shape_predictor_68_face_landmarks.dat"
	predictor = dlib.shape_predictor(p) 
	#Pupil detector is used to detect cercles(so pupils) in the eye frame
	detector_params = cv2.SimpleBlobDetector_Params()
	detector_params.filterByArea = True
	detector_params.maxArea = 1500
	detector_pupil = cv2.SimpleBlobDetector_create(detector_params)	
	return detector, predictor, detector_pupil

detector, predictor, detector_pupil = init_cv()

def init_var_eye():
	#Retuern points' indices that define the eyes in the face' landmarks list
	LEFT_EYE_POINTS = [36, 37, 38, 39, 40, 41]
	RIGHT_EYE_POINTS = [42, 43, 44, 45, 46, 47]
	return LEFT_EYE_POINTS, RIGHT_EYE_POINTS

LEFT_EYE_POINTS, RIGHT_EYE_POINTS = init_var_eye()

def init_var_choices():
	"""
	Intialize variables used for questions and choices' selection
	"""

	#Variables used in choice detection
	choice_index = None
	choices = {1:None,2:None}
	choices_map = {"left":1, "right":2}
	#Variables used in question selection
	question_index = 0
	questions_number = len(questions_names)	
	#Current question
	question_current = questions_names[question_index]
	#The list of answered questions and its answers
	questions_answered = []
	#List of the current question' choices
	text_left = questions_choices[question_index][0]
	text_right = questions_choices[question_index][1]
	choices[1] = text_left
	choices[2] = text_right
	choices_images = {1:None, 2:None}
	#List of the current question'choices images
	image_left = cv2.imread(questions_images[question_index][0])
	image_right = cv2.imread(questions_images[question_index][1])
	choices_images[1] = image_left
	choices_images[2] = image_right  
	#Theduration of gazing
	duration = 0
	
	return questions_answered, question_current, question_index, questions_number, choice_index, choices, choices_map, text_left, text_right, choices,\
	choices_images, image_left, image_right, duration, questions_names, questions_images, questions_choices, questions_next,\
	questions_choices_notifications

questions_answered, question_current, question_index, questions_number, choice_index, choices, choices_map, text_left, text_right, choices,\
choices_images, image_left, image_right, duration, questions_names, questions_images, questions_choices, questions_next,\
questions_choices_notifications = init_var_choices()


def init_board(screen_width, screen_height):
	"""
	Initialize the board
	The board is the main image for showing questions and choices
	"""
	board = np.full((screen_width, screen_height,3), 255, np.uint8)
	cell_text = ""
	font_letter = cv2.FONT_HERSHEY_TRIPLEX
	font_scale = 3
	font_th = 4
	write_text(cell_text, font_letter, font_scale, font_th, screen_height, screen_width, 0, 0, board)
	return board

def gaze_classifying_rl(horizontal_ratio, l_limit, r_limit):
	"""
	Return the directin of gaze

	Arguments :
		horizontal_ratio (float): the ratio of gaze_x_coordinate/eye_width
		l_limit (float): the limit after which the gaze is classified looking left
		r_limit (float): the limit before which the gaze is classified looking right
	"""
	if (horizontal_ratio > l_limit):
		text = "left"
	elif (horizontal_ratio < r_limit):
		text = "right"
	else:
		text = None
	return text


def detect_eye(points, landmarks, gray):
	"""
	Arguments:
		points (list): the points' indices difining the eye region in the facial landmarks
		landmarks (dlib.full_object_detection) : the coordiantes of facial landmarks in the face frame
		gray (numpy.ndarray) : the face frame in a grayscale
	Return:
		eye (np.ndarray): A frame containing the eye and nothing else
		region ((np.ndarray)) : Contains the coordinates of the eyes landmarks in the face frame 
		eye_center  (tuple) : The eye center coordinates in the eye frame
	"""
	#The indices of the points defininf the eye contour in teh facial landmarks
	region = np.array([(landmarks.part(point).x, landmarks.part(point).y) for point in points])
	region = region.astype(np.int32)
	#Get the eye frame By applying a mask
	height, width = gray.shape[:2]
	black_frame = np.zeros((height,width), np.uint8)
	mask = np.full((height,width), 255, np.uint8) # matrix of 255 (white pixels)
	cv2.fillPoly(mask,[region],(0,0,0))
	eye = cv2.bitwise_not(black_frame, gray.copy(), mask = mask)
	#Crop the eye from the face frame
	margin = 5
	min_x = np.min(region[:,0]) - margin
	max_x = np.max(region[:,0]) + margin
	min_y = np.min(region[:,1]) - margin
	max_y = np.max(region[:,1]) + margin
	eye = eye[min_y:max_y, min_x:max_x]
	#The eye center coordinates in the eye frame
	eye_center = (int(eye.shape[:2][1]/2), int(eye.shape[:2][0]/2))
	return eye, region, eye_center


def draw_eye_center(eye_filtered, eye_center):
	"""
	Draw the eye center as a white point in the filtered eye frame
	"""
	cv2.circle(eye_filtered, (eye_center[0], eye_center[1]), 2, (255, 255, 255), -1)
	return eye_filtered

def mid_vals(p1,p2):
	"""
	Return the middle value between two values	
	"""
	if (p1):
		if(p2):
			return (p1 + p2)/2
		else:
			return(p1)
	else:
		if(p2):
			return p2
		else:
			return None

#apply some filters on the eye frame in order to remove noise
def image_processing_git(eye_frame):
	"""
	Performes operations on the eye frame to isolate the iris
	This method use the bilateral filter to remove noise from the eye frame

	Arguments:
		eye_frame (np.ndarray): A frame containing the eye and nothing else
	"""
	kernel = np.ones((3,3), np.uint8)
	new_frame = cv2.bilateralFilter(eye_frame, 10, 15, 15)
	new_frame = cv2.erode(new_frame, kernel, iterations = 3)
	return new_frame

def image_processing_med_thresh(eye_frame, threshold):
	"""
	Performes operations on the eye frame to isolate the iris
	This method use the Median Blur filter to remove noise from the eye frame

	Arguments:
		eye_frame (np.ndarray): A frame containing the eye and nothing else
		threshold (int) : Threshold value used to binarize the eye frame
	"""
	_, new_frame = cv2.threshold(eye_frame, threshold, 255, cv2.THRESH_BINARY)
	new_frame = cv2.erode(new_frame, None, iterations=2)
	new_frame = cv2.dilate(new_frame, None, iterations=4)
	new_frame = cv2.medianBlur(new_frame, 5)
	return new_frame

def image_processing_med_thresh_modif(eye_frame, threshold):
	"""
	Performes operations on the eye frame to isolate the iris
	This method use the Median Blur filter to remove noise from the eye frame but with different morphological operations

	Arguments:
		eye_frame (np.ndarray): A frame containing the eye and nothing else
		threshold (int) : Threshold value used to binarize the eye frame
	"""
	kernel = np.ones((3,3), np.uint8)
	new_frame = cv2.bilateralFilter(eye_frame, 10, 15, 15)
	new_frame = cv2.erode(new_frame, kernel, iterations = 3)
	_, new_frame = cv2.threshold(new_frame, threshold,	 255, cv2.THRESH_BINARY)
	return new_frame

def image_processing_git_thresh(eye_frame, threshold):
	"""
	Performes operations on the eye frame to isolate the iris
	This method use the bilateral filter to remove noise from the eye frame

	Arguments:
		eye_frame (np.ndarray): A frame containing the eye and nothing else
		threshold (int) : Threshold value used to binarize the eye frame
	"""
	kernel = np.ones((3,3), np.uint8)
	new_frame = cv2.bilateralFilter(eye_frame, 10, 15, 15)
	new_frame = cv2.erode(new_frame, kernel, iterations = 3)
	_, new_frame = cv2.threshold(new_frame, threshold,	 255, cv2.THRESH_BINARY)
	return new_frame
#detect the and return its associated image

#detect the pupil
def pupil_med(img, threshold, eye_center):
	"""
	Detect the pupil using the circle detection algorithm
	
	Arguments :
		img (np.ndarray): A frame containing the eye and nothing else
		threshold (int) : Threshold value used to binarize the eye frame
		eye_center  (tuple) : The eye center coordinates in the eye frame
	Return:
		pupil_coords (tuple) : the coordination of the pupil in teh eye frame 
		img : the processed eye frame
	"""
	#Apply some operations on teh image in order to remove noises
	img = image_processing_med_thresh(img, threshold)
	pupil_coords = None
	#Detect the circles in the image
	keypoints = detector_pupil.detect(img)
	#print(len(keypoints))
	if keypoints != []:
		keypoints.sort(key = lambda x:x.size, reverse = True)
		(x,y) = keypoints[0].pt
		print(keypoints[0].size)
		pupil_coords = (x, y)
		#Draw the detected circle on the eye frame
		img = cv2.drawKeypoints(img, keypoints, img, (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
	#Draw the eye center on teh eye frame
	img = draw_eye_center(img, eye_center)
	return pupil_coords, img

def pupil_git(img, threshold, eye_center):
	"""
	Detect the pupil using the contouur detection algorithm
	
	Arguments :
		img (np.ndarray): A frame containing the eye and nothing else
		threshold (int) : Threshold value used to binarize the eye frame
		eye_center  (tuple) : The eye center coordinates in the eye frame
	Return:
		pupil_coords (tuple) : the coordination of the pupil in teh eye frame 
		img : the processed eye frame
	"""
	#Apply some operations on teh image in order to remove noises
	img = image_processing_git_thresh(img, threshold)
	#Detect the contours in the image
	contours,t = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
	contours = sorted(contours, key=cv2.contourArea)
	pupil_coords = None
	try:
		moments = cv2.moments(contours[-2])
		x  = int(moments['m10'] / moments['m00'])
		y  = int(moments['m01'] / moments['m00'])
		pupil_coords = (x, y)
		for contour in contours:
			img = cv2.drawContours(img, contour, -1, (255,255,255), 1)		
	except (IndexError, ZeroDivisionError):
		pass

	return pupil_coords, img

def detect_pupil(eye_center, eye, threshold):
	"""
	Calculate the horizontal ratio = pupil x_coordinate/eye_width
	Draw pupil' centers
	"""
	#Get the pupil coordianations
	pupil_coords, eye_filtered = pupil_git(eye, threshold, eye_center)
	horizontal_ratio = None
	#Draw pupil center
	if (pupil_coords):
		(x,y) = pupil_coords
		(x,y) = (int(x),int(y))
		color = (255,255,255)
		cv2.line(eye_filtered, (x - 5, y), (x + 5, y), color)
		cv2.line(eye_filtered, (x, y - 5), (x, y + 5), color)
		#Calculate the horizontal ratio = pupil x_coordinate/eye_width
		horizontal_ratio = x/(eye_center[0]*2)
	return horizontal_ratio, eye_filtered 

#CALIBRATING
#The number of frames processed for calibbration
nb_frames = 10

def is_complete(thresholds_left, thresholds_right):
	"""
	Verify if the calibration is complete
	The calibration is complete when teh value of threshols is calcuated on nb_frames frames
	"""
	return (len(thresholds_left) >= nb_frames) and (len(thresholds_right) >= nb_frames) 

def threshold(thresholds):
	"""
	Return the average value of threshold from the nb_frames values calculated	
	"""
	return int(sum(thresholds)/len(thresholds))

def iris_size(frame):
	"""
	Return the iris size
	"""
	frame = frame[5:-5, 5:-5]
	height, width  = frame.shape[:2]
	nb_pixels = height*width
	nb_blacks = nb_pixels - cv2.countNonZero(frame)
	return nb_blacks/nb_pixels

def find_best_threshold(eye_frame):
	"""
	Return the best value of threshold : the one taht result a ratio of iris size near to the average iris size (0.48) 
	"""
	average_iris_size = 0.48
	trials = {}
	for threshold in range(5, 100, 5):
		iris_frame = image_processing_git_thresh(eye_frame, threshold)
		trials[threshold] = iris_size(iris_frame)
	best_threshold, irs = min(trials.items()  , key =(lambda p: abs(p[1] - average_iris_size)) )
	return best_threshold

def calibrate(eye_frame, thresholds):
	"""
	Calibrate the threshold
	"""
	threshold = find_best_threshold(eye_frame)
	thresholds.append(threshold)

def get_nb_frames():
	"""
	Return the number of frames in the camera
	"""
	num_frames = 120	
	start = time.time()
	for i in range(0,num_frames):
		_,frame = cap.read()
	end  = time.time()
	seconds = end - start
	fps = int(num_frames/seconds)
	return fps

#drawing the cells of the choices map

def write_text(text, font_letter, font_scale, font_th, cell_width, cell_height, cell_x, cell_y, board):
	"""
	Function used to write a text on an image

	Arguments:
		text (str) : The text to write
		font_letter (int) : The text's font
		font_scale (int) : The text's scale
		font_th (int) : The text's thickness
		cell_width (int) : width of the rectangle in wich the text will be written
		cell_height (int) : height of the rectangle in wich the text will be written
		cell_x (int) : x coordinate of the rectangle in wich the text will be written
		cell_y (int) : y coordinate of the rectangle in wich the text will be written
		board : the image in wich the text is written
	"""
	text_size = cv2.getTextSize(text, font_letter, font_scale, font_th)[0]
	width_text, height_text = text_size[0], text_size[1]
	text_x = int((cell_width - width_text)/2) + cell_x
	text_y = int((cell_height - height_text)/2) + cell_y
	cv2.putText(board, text, (text_x, text_y), font_letter, font_scale, (0, 0, 0), font_th)	

def cell(cell_number, cell_text, letter_light, board,img):
	"""
	Draw the choices rectangles(cells) on the board
	Arguments:
		cell_number (int) : the number of the cell 
		cell_text (str) : The text to write
		letter_light (bool) : indicates if the user is looking at the cell or not 
		board (np.ndarray) : The image in wich the text is written
		img (np.ndarray) : The image associated with the cell's whoice
	"""
	#Defining cells dimensions with coreesponding to the screen dimensions
	cell_width = int(screen_width/3)
	cell_height = int(screen_height/3)
	cell_th = 3
	#Defining cells positions
	if (cell_number == 1):
		cell_x = 0
		cell_y = cell_height
	elif(cell_number == 2):
		cell_x = cell_width*2
		cell_y = cell_height
	#Drawing rectangles
	if (letter_light == True):
		cv2.rectangle(board,(cell_x+cell_th,cell_y+cell_th),(cell_x +cell_width-cell_th,cell_y +cell_height+cell_th),(102,178,255),-1)
	else:
		cv2.rectangle(board,(cell_x+cell_th,cell_y+cell_th),(cell_x +cell_width-cell_th,cell_y +cell_height+cell_th),(255,255,255),-1)
	#Draw a circle at wich the user must look in order to choose
	cv2.circle(board,(cell_x + int(cell_width/2) , int(screen_height/2)), 30, (255,255, 102), -1)
	#Showing the choice's image
	if img is not None:
		img = cv2.resize(img,(cell_height,cell_height))
		if cell_number == 1:
			board[cell_height:cell_height+ cell_height,int(cell_width/4):cell_height + int(cell_width/4)] = img
		elif cell_number ==2:
			board[cell_height:cell_height+ cell_height,cell_x + int(cell_width/4):cell_x + cell_height + int(cell_width/4)] = img
	else:
		#Writing the choice
		font_letter = cv2.FONT_HERSHEY_PLAIN
		font_scale = 3
		font_th = 4
		write_text(cell_text, font_letter, font_scale, font_th, cell_width, cell_height, cell_x, cell_y, board)


def cell_ask(cell_number, cell_text, letter_light, board):
	"""
	Draw the choices rectangles(cells) on the board for the "Ask a question" section
	"""
	cell_width = int(screen_width/3)
	cell_height = int(screen_height/3)
	cell_th = 3

	if (cell_number == 1):
		cell_x = 0
		cell_y = cell_height
	elif(cell_number == 2):
		cell_x = cell_width*2
		cell_y = cell_height

	if (letter_light == True):
		cv2.rectangle(board,(cell_x+cell_th,cell_y+cell_th),(cell_x +cell_width-cell_th,cell_y +cell_height+cell_th),(190,119,0),-1)
	else:
		cv2.rectangle(board,(cell_x+cell_th,cell_y+cell_th),(cell_x +cell_width-cell_th,cell_y +cell_height+cell_th),(255,255,255),-1)

	font_letter = cv2.FONT_HERSHEY_PLAIN
	font_scale = 3
	font_th = 4
	write_text(cell_text, font_letter, font_scale, font_th, cell_width, cell_height, cell_x, cell_y, board)

















