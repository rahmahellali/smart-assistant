"""
This code implements the differents componenets ofthe GUI
The differents parts of the GUI (buttons, images .. ) or a function of
the screen size
"""
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import *
from PyQt5.uic import loadUi
import sys
import map_face_process as process

class Ui_Main(QtWidgets.QWidget):
    """
    A class that defines the GUI
    The GUI was first built for a scrren size of (1296,700),
    I introduces 2 scales factors, on for each dimension, (rf_x,rf_y),
    in order to have a GUI resizable for many screen sizes

    The GUI is formed from many pages, called stacks,
    each stack has his own componenets (buttons,images ..).
    
    The function "setupUi_stack" define the component of the stack, and
    the function "retranslateUi_stack" defines the names of the components.
    
    The GUI of each stack is done usung the software QtDesigner and the code of
    "setupUi_stack" and "retranslateUi_stack" is generated using the command
    puic5 [file.ui] > [newFileName.py]

    The stacks are stored in a list called a Stack Layout.

    The names of the different components:
        * QStackedLayout : A stack Layout
        * QtStack : A stack
        * QLabel : A Label
        * QPushButton : A button

    """
    def setupUi(self, MainWindow):
        """
        A function that setup the GUI
        """
        #Get the screen size
        self.screen_width = process.screen_width
        self.screen_height = process.screen_height
        
        #Set the referencial screen size
        self.reference_screen_width = 1296
        self.reference_screen_height = 700
        
        #Th Scale factors
        self.rf_x = self.screen_width/self.reference_screen_width
        self.rf_y = self.screen_height/self.reference_screen_height

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(self.screen_width, self.screen_height)
        
        #Initialize the stack widget
        self.QtStack = QtWidgets.QStackedLayout()
        
        #Initializing stacks
        self.stack1 = QtWidgets.QWidget() #main
        self.stack2 = QtWidgets.QWidget() #patient
        self.stack3 = QtWidgets.QWidget() #guided
        self.stack4 = QtWidgets.QWidget() #assistant
        self.stack5 = QtWidgets.QWidget() #ask a question
        self.stack6 = QtWidgets.QWidget() #edit parameters
        self.stack7 = QtWidgets.QWidget() #automatic
        self.stack8 = QtWidgets.QWidget() #solution2
        

        #Define every stack GUI
        self.setupUi_stack_1()
        self.setupUi_stack_2()
        self.setupUi_stack_3()
        self.setupUi_stack_4()
        self.setupUi_stack_5()
        self.setupUi_stack_6()
        self.setupUi_stack_7()
        self.setupUi_stack_8()

        #Add each stack to the Stack Layout
        self.QtStack.addWidget(self.stack1)
        self.QtStack.addWidget(self.stack2)
        self.QtStack.addWidget(self.stack3)
        self.QtStack.addWidget(self.stack4)
        self.QtStack.addWidget(self.stack5)
        self.QtStack.addWidget(self.stack6)
        self.QtStack.addWidget(self.stack7)
        self.QtStack.addWidget(self.stack8)

    #Stack 1 : The Main stack

    def setupUi_stack_1(self):
        """
        This function defines the components of the first stack
        This stack contains 2 buttons : 
            *Patient Mode Button: redirects the user to the Patient stack
            *Assitant Mode Button : redirects the user to the Assistant stack
        and an image "baseImage_1" which defines the board in wich we write informations, questions, choices .. etc,
        in this case we write on the board "GAZE TRACKER".
        """
        self.stack1.resize(self.screen_width, int(680*self.rf_y))
        #Setting the board. 
        self.baseImage_1 = QtWidgets.QLabel(self.stack1)
        self.baseImage_1.setGeometry(QtCore.QRect(0, int(40*self.rf_y), int(1291*self.rf_x), int(640*self.rf_y)))
        self.baseImage_1.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_1.setText("")
        self.baseImage_1.setObjectName("baseImage_1")
        #Titre application
        self.label1 = QtWidgets.QLabel(self.stack1)
        self.label1.setFont(QtGui.QFont("Titre", 12, QtGui.QFont.Bold))
        self.label1.setGeometry(550, 20, 300, 200)
        #Assitant Button
        self.assistantButton = QtWidgets.QPushButton(self.stack1)
        self.assistantButton.setGeometry(QtCore.QRect(int(700*self.rf_x),int(150*self.rf_y), int(300*self.rf_x), int(400*self.rf_y)))
        self.assistantButton.setIcon(QtGui.QIcon('medecin.png'))
        self.assistantButton.setIconSize(QSize(270,270))
        self.assistantButton.setObjectName("assistantButton")
        #Patient Button
        self.patientButton = QtWidgets.QPushButton(self.stack1)
        self.patientButton.setGeometry(QtCore.QRect(int(300*self.rf_x), int(150*self.rf_y), int(300*self.rf_x), int(400*self.rf_y)))
        self.patientButton.setIcon(QtGui.QIcon('patient.png'))
        self.patientButton.setIconSize(QSize(270,270))
        self.patientButton.setObjectName("patientButton")
        #Set the names of the buttons
        self.retranslateUi_stack_1()

    def retranslateUi_stack_1(self):
        """
        A function that set the names of the buttons
        """
        self.label1.setText("Communicate with your eyes")
        self.assistantButton.setText("")
        self.patientButton.setText("")

    #Stack 2 : The Patient Mode stack

    def setupUi_stack_2(self):
        """
        This function defines the components of the second stack
        This stack contains 3 buttons : 
            *Guided Mode Button: redirects the user to the Guided stack
            *Automatic Mode Button : redirects the user to the Automatic stack
            *Main Button : redirects the user to the first stack
        and an image "baseImage_2" which defines the board in wich we write "GAZE TRACKER".
        """
        self.stack2.resize(self.screen_width, int(680*self.rf_y))
        #Setting the board.
        self.baseImage_2 = QtWidgets.QLabel(self.stack2)
        self.baseImage_2.setGeometry(QtCore.QRect(0, int(40*self.rf_y), int(1291*self.rf_x), int(640*self.rf_y)))
        self.baseImage_2.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_2.setText("")
        self.baseImage_2.setObjectName("baseImage_2")
        #Titre application
        self.label2 = QtWidgets.QLabel(self.stack2)

        self.label2.setFont(QtGui.QFont("Titre", 12, QtGui.QFont.Bold))
        self.label2.setGeometry(550, 20, 300, 200)



        #Automatic Mode Button
        self.automaticButton = QtWidgets.QPushButton(self.stack2)
        self.automaticButton.setGeometry(QtCore.QRect(int(100*self.rf_x), 0, int(300*self.rf_x), int(31*self.rf_y)))
        self.automaticButton.setObjectName("automaticButton")

        #solution2 Mode Button
        self.solution2Button = QtWidgets.QPushButton(self.stack2)
        self.solution2Button.setGeometry(QtCore.QRect(int(430*self.rf_x), 0, int(300*self.rf_x), int(31*self.rf_y)))
        self.solution2Button.setObjectName("solution2Button")

        #Guided Mode Button 
        self.guidedButton = QtWidgets.QPushButton(self.stack2)
        self.guidedButton.setGeometry(QtCore.QRect(int(760*self.rf_x), 0, int(300*self.rf_x), int(31*self.rf_y)))
        self.guidedButton.setObjectName("guidedButton")

        #Main Button
        self.mainButton_1 = QtWidgets.QPushButton(self.stack2)
        self.mainButton_1.setGeometry(QtCore.QRect(int(1100*self.rf_x), 0, int(50*self.rf_x), int(38*self.rf_y)))
        self.mainButton_1.setIcon(QtGui.QIcon('main.png'))
        self.mainButton_1.setIconSize(QSize(35,35))
        self.mainButton_1.setObjectName("mainButton_1")
        #Set the names of the buttons
        self.retranslateUi_stack_2()

    def retranslateUi_stack_2(self):
        """
        A function that set the names of the buttons
        """
        self.label2.setText("Communicate with your eyes")
        self.guidedButton.setText("Guided")
        self.automaticButton.setText("One Choice App")
        self.solution2Button.setText("List Choice App")
        self.mainButton_1.setText("")

    #Stack 3 : The Guided Mode stack

    def setupUi_stack_3(self):
        """
        This function defines the components of the third stack
        This stack contains 4 buttons : 
            *Start Button: start the guided mode ( show a set of "question and its choices" to the user,
            and allow him to choose)
            *Next Button : redirect the user to the next question after he answered a question with YES,
            the assistant press this button after finishing th service chosen by the patient.
            *Stop Button : stop the guided mode and show the main image (that have "GAZE TRACKER" text).
            *Main Button : redirects the user to the first stack.
        and an image "baseImage_3" which defines the board in wich we write, questions, choices and notifications.
        """
        self.stack3.resize(self.screen_width, int(680*self.rf_y))
        #Setting the board.
        self.baseImage_3 = QtWidgets.QLabel(self.stack3)
        self.baseImage_3.setGeometry(QtCore.QRect(0, int(40*self.rf_y), int(1291*self.rf_x), int(640*self.rf_y)))
        self.baseImage_3.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_3.setText("")
        self.baseImage_3.setObjectName("baseImage_3")
        # Start Button 
        self.startButton = QtWidgets.QPushButton(self.stack3)
        self.startButton.setGeometry(QtCore.QRect(int(200*self.rf_x), 0, int(50*self.rf_x), int(38*self.rf_y)))
        self.startButton.setIcon(QtGui.QIcon('start.png'))
        self.startButton.setIconSize(QSize(35,35))
        self.startButton.setObjectName("startButton")
        # Next Button 
        self.nextButton = QtWidgets.QPushButton(self.stack3)
        self.nextButton.setGeometry(QtCore.QRect(int(450*self.rf_x), 0, int(50*self.rf_x), int(38*self.rf_y)))
        self.nextButton.setIcon(QtGui.QIcon('next.png'))
        self.nextButton.setIconSize(QSize(35,35))
        self.nextButton.setObjectName("nextButton")
        # Stop Button 
        self.stopButton = QtWidgets.QPushButton(self.stack3)
        self.stopButton.setGeometry(QtCore.QRect(int(700*self.rf_x), 0, int(50*self.rf_x), int(38*self.rf_y)))
        self.stopButton.setIcon(QtGui.QIcon('stop.png'))
        self.stopButton.setIconSize(QSize(35,35))
        self.stopButton.setObjectName("stopButton")
        # Main Button 
        self.mainButton_2 = QtWidgets.QPushButton(self.stack3)
        self.mainButton_2.setGeometry(QtCore.QRect(int(950*self.rf_x), 0, int(50*self.rf_x), int(38*self.rf_y)))
        self.mainButton_2.setIcon(QtGui.QIcon('main.png'))
        self.mainButton_2.setIconSize(QSize(35,35))
        self.mainButton_2.setObjectName("mainButton_2")
        #Set the names of the buttons
        self.retranslateUi_stack_3()

    def retranslateUi_stack_3(self):
        """
        A function that set the names of the buttons
        """
        self.startButton.setText("")
        self.nextButton.setText("")
        self.stopButton.setText("")
        self.mainButton_2.setText("")

    #Stack 4 : The Assistant Mode stack

    def setupUi_stack_4(self):
        """
        This function defines the components of the fourth stack
        This stack defines the services available for the assistant through the software :
            *Edit paramaters : Edit some parameters
            *Ask a question : Ask a customized question 
        This stack contains 4 buttons : 
            *Edit Button : Allow the assistant to change the values of the parameters "Dwelling time" 
            and "question time" 
            *Ask a question : redirect the user to the "Ask a question" stack
            *Main Button : redirects the user to the first stack.
        and an image "baseImage_3" which defines the board in wich we write, questions, choices and notifications.
        """
        self.stack4.resize(self.screen_width, int(680*self.rf_y))
        #Setting the board.
        self.baseImage_4 = QtWidgets.QLabel(self.stack4)
        self.baseImage_4.setGeometry(QtCore.QRect(0, int(40*self.rf_y), int(1291*self.rf_x), int(640*self.rf_y)))
        self.baseImage_4.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_4.setText("")
        self.baseImage_4.setObjectName("baseImage_4")
        # Edit Paramatetrs Button 
        self.editButton = QtWidgets.QPushButton(self.stack4)
        self.editButton.setGeometry(QtCore.QRect(int(110*self.rf_x), 0, int(300*self.rf_x), int(31*self.rf_y)))
        self.editButton.setObjectName("editButton")
        # Ask a question Button 
        self.askButton = QtWidgets.QPushButton(self.stack4)
        self.askButton.setGeometry(QtCore.QRect(int(530*self.rf_x), 0, int(300*self.rf_x), int(31*self.rf_y)))
        self.askButton.setObjectName("askButton")
        # Main Button
        self.mainButton_3 = QtWidgets.QPushButton(self.stack4)
        self.mainButton_3.setGeometry(QtCore.QRect(int(940*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.mainButton_3.setIcon(QtGui.QIcon('main.png'))
        self.mainButton_3.setIconSize(QSize(35,35))
        self.mainButton_3.setObjectName("mainButton_3")
        #Set the names of the buttons
        self.retranslateUi_stack_4()

    def retranslateUi_stack_4(self):
        """
        A function that set the names of the buttons
        """
        self.editButton.setText("Edit Parameters")
        self.askButton.setText("Ask a question")
        self.mainButton_3.setText("")

    #Stack 5 : The Ask a Question stack

    def setupUi_stack_5(self):
        """
        This function defines the components of the fifth stack
        This stack allow the assistant to ask a customized question : 
        it allows him to write a question and to choices.

        This stack contains 3 line Edit field (fields in wich the user can write a text):
            * question_lineEdit : field in wich the assistant write th question
            * choice_1_lineEdit : field in wich the assistant write th left choice
            * choice_2_lineEdit : field in wich the assistant write th right choice

        ,2 buttons : 
            *Start Button : By pushing the button:
                - The question is shown on the board for a "question time" duration
                - The choices are shown
                - After the patient choose, the answer is shown 
            *Main Button : redirects the user to the first stack.

        ,3 labels : text that indicate the field next to it.

        and an image "baseImage_ask" which defines the board in wich we write question, choices and answer.
        """
        self.stack5.resize(self.screen_width, int(653*self.rf_y))
        #Setting the board.
        self.baseImage_ask = QtWidgets.QLabel(self.stack5)
        self.baseImage_ask.setGeometry(QtCore.QRect(0, int(60*self.rf_y), int(1291*self.rf_x), int(690*self.rf_y)))
        self.baseImage_ask.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_ask.setText("")
        self.baseImage_ask.setObjectName("baseImage_ask")
        #Question label
        self.question_label = QtWidgets.QLabel(self.stack5)
        self.question_label.setGeometry(QtCore.QRect(int(290*self.rf_x), 0, int(67*self.rf_x), int(30*self.rf_y)))
        self.question_label.setObjectName("question_label")
        #Question field
        self.question_lineEdit = QtWidgets.QLineEdit(self.stack5)
        self.question_lineEdit.setGeometry(QtCore.QRect(int(370*self.rf_x), 0, int(581*self.rf_x), int(30*self.rf_y)))
        self.question_lineEdit.setObjectName("question_lineEdit")
        #Left choice label
        self.choice_1_label = QtWidgets.QLabel(self.stack5)
        self.choice_1_label.setGeometry(QtCore.QRect(int(300*self.rf_x), int(30*self.rf_y), int(67*self.rf_x), int(30*self.rf_y)))
        self.choice_1_label.setObjectName("choice_1_label")
        #Left choice field
        self.choice_1_lineEdit = QtWidgets.QLineEdit(self.stack5)
        self.choice_1_lineEdit.setGeometry(QtCore.QRect(int(380*self.rf_x), int(30*self.rf_y), int(221*self.rf_x), int(30*self.rf_y)))
        self.choice_1_lineEdit.setObjectName("choice_1_lineEdit")
        #Right choice label
        self.choice_2_label = QtWidgets.QLabel(self.stack5)
        self.choice_2_label.setGeometry(QtCore.QRect(int(640*self.rf_x), int(30*self.rf_y), int(67*self.rf_x), int(30*self.rf_y)))
        self.choice_2_label.setObjectName("choice_2_label")
        #Right choice field
        self.choice_2_lineEdit = QtWidgets.QLineEdit(self.stack5)
        self.choice_2_lineEdit.setGeometry(QtCore.QRect(int(720*self.rf_x), int(30*self.rf_y), int(221*self.rf_x), int(30*self.rf_y)))
        self.choice_2_lineEdit.setObjectName("choice_2_lineEdit")
        #Start Button
        self.startButton_ask = QtWidgets.QPushButton(self.stack5)
        self.startButton_ask.setGeometry(QtCore.QRect(int(960*self.rf_x), int(30*self.rf_y), int(99*self.rf_x), int(27*self.rf_y)))
        self.startButton_ask.setObjectName("startButton_ask")
        #Main button
        self.mainButton_ask = QtWidgets.QPushButton(self.stack5)
        self.mainButton_ask.setGeometry(QtCore.QRect(int(1060*self.rf_x), int(30*self.rf_y), int(50*self.rf_x), int(40*self.rf_y)))
        self.mainButton_ask.setIcon(QtGui.QIcon('main.png'))
        self.mainButton_ask.setIconSize(QSize(35,35))
        self.mainButton_ask.setObjectName("mainButton_ask")
        #Set the names of the buttons
        self.retranslateUi_stack_5()

    def retranslateUi_stack_5(self):
        """
        A function that set the names of the buttons
        """
        self.question_label.setText("Question")
        self.choice_1_label.setText("Choice 1")
        self.choice_2_label.setText("Choice 2")
        self.startButton_ask.setText("Start")
        self.mainButton_ask.setText("")

    #Stack 6 : The Edit Paramaters stack

    def setupUi_stack_6(self):
        """
        This function defines the components of the sicth stack
        This stack allow the assistant to ask a customized question : 
        it allows him to write a question and to choices.

        This stack contains 3 line Edit field (fields in wich the user can write a text):
            * dwelling_lineEdit : field in wich the assistant write the dwelling time (which is by default 2)
            * question_time_lineEdit : field in wich the assistant write the question time (which is by default 2)

        ,2 buttons : 
            *Submit Button : By pushing the button the values of the paramaters are changed with the new values
            *Main Button : redirects the user to the first stack.

        ,2 labels : text that indicate the field next to it.

        and an image "baseImage_ask" which defines the board in wich we write question, choices and answer.
        """
        self.stack6.resize(self.screen_width, int(653*self.rf_y))
        #Setting the board.
        self.baseImage_edit = QtWidgets.QLabel(self.stack6)
        self.baseImage_edit.setGeometry(QtCore.QRect(0, int(39*self.rf_y), int(1291*self.rf_x), int(611*self.rf_y)))
        self.baseImage_edit.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_edit.setText("")
        self.baseImage_edit.setObjectName("baseImage_edit")
        #Dwelling time label
        self.dwelling_label = QtWidgets.QLabel(self.stack6)
        self.dwelling_label.setGeometry(QtCore.QRect(int(50*self.rf_x), 0, int(101*self.rf_x), int(30*self.rf_y)))
        self.dwelling_label.setObjectName("dwelling_label")
        #Dwelling time field
        self.dwelling_lineEdit = QtWidgets.QLineEdit(self.stack6)
        self.dwelling_lineEdit.setGeometry(QtCore.QRect(int(150*self.rf_x), 0, int(41*self.rf_x), int(31*self.rf_y)))
        self.dwelling_lineEdit.setObjectName("dwelling_lineEdit")       
        #Question time label
        self.question_time_label = QtWidgets.QLabel(self.stack6)
        self.question_time_label.setGeometry(QtCore.QRect(int(210*self.rf_x), 0, int(101*self.rf_x), int(30*self.rf_y)))
        self.question_time_label.setObjectName("question_time_label")
        #Question time field
        self.question_time_lineEdit = QtWidgets.QLineEdit(self.stack6)
        self.question_time_lineEdit.setGeometry(QtCore.QRect(int(310*self.rf_x), 0, int(41*self.rf_x), int(31*self.rf_y)))
        self.question_time_lineEdit.setObjectName("question_time_lineEdit")
        #Submit paramaters button
        self.submit_parameters_Button = QtWidgets.QPushButton(self.stack6)
        self.submit_parameters_Button.setGeometry(QtCore.QRect(int(370*self.rf_x), 0, int(99*self.rf_x), int(31*self.rf_y)))
        self.submit_parameters_Button.setObjectName("submit_parameters_Button")        
        #Main button
        self.mainButton_edit = QtWidgets.QPushButton(self.stack6)
        self.mainButton_edit.setGeometry(QtCore.QRect(int(480*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.mainButton_edit.setIcon(QtGui.QIcon('main.png'))
        self.mainButton_edit.setIconSize(QSize(35,35))
        self.mainButton_edit.setObjectName("mainButton_edit")
        #Set the names of the buttons
        self.retranslateUi_stack_6()

    def retranslateUi_stack_6(self):
        """
        A function that set the names of the buttons
        """
        self.dwelling_label.setText("Dwelling time")
        self.question_time_label.setText("Question time")
        self.submit_parameters_Button.setText("Submit")
        self.mainButton_edit.setText("")

    def setupUi_stack_7(self):
        """
        This function defines the components of the seventh stack
        This stack contains 4 buttons : 
            *Start Button: start the automatic mode ( implementing the reinforcement learning)
            *Next Button : redirect the user to the next question after he answered a question with YES,
            the assistant press this button after finishing th service chosen by the patient.
            *Stop Button : stop the automatic mode and show the main image (that have "GAZE TRACKER" text).
            *Main Button : redirects the user to the first stack.
        and an image "baseImage_auto" which defines the board in wich we write, questions, choices and notifications.
        """
        self.stack7.resize(self.screen_width, int(680*self.rf_y))
        #Setting the board.
        self.baseImage_auto = QtWidgets.QLabel(self.stack7)
        self.baseImage_auto.setGeometry(QtCore.QRect(0, int(40*self.rf_y), int(1291*self.rf_x), int(640*self.rf_y)))
        self.baseImage_auto.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_auto.setText("")
        self.baseImage_auto.setObjectName("baseImage_7")
        #Titre application
        # self.label3 = QtWidgets.QLabel(self.stack7)
        # self.label3.setFont(QtGui.QFont("Titre", 12, QtGui.QFont.Bold))
        # self.label3.setGeometry(1100,10, 300, 200)
        #Start Button
        self.startButtonAuto = QtWidgets.QPushButton(self.stack7)
        self.startButtonAuto.setGeometry(QtCore.QRect(int(450*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.startButtonAuto.setIcon(QtGui.QIcon('start.png'))
        self.startButtonAuto.setIconSize(QSize(40,40))
        self.startButtonAuto.setObjectName("startButtonAuto")
        #Next Button
        self.nextButtonAuto = QtWidgets.QPushButton(self.stack7)
        self.nextButtonAuto.setGeometry(QtCore.QRect(int(550*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.nextButtonAuto.setIcon(QtGui.QIcon('Next.png'))
        self.nextButtonAuto.setIconSize(QSize(40,40))
        self.nextButtonAuto.setObjectName("nextButtonAuto")
        #Stop Button
        self.stopButtonAuto = QtWidgets.QPushButton(self.stack7)
        self.stopButtonAuto.setGeometry(QtCore.QRect(int(650*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.stopButtonAuto.setIcon(QtGui.QIcon('stop.png'))
        self.stopButtonAuto.setIconSize(QSize(40,40))
        self.stopButtonAuto.setObjectName("stopButtonAuto")
        #Main Button
        self.mainButtonAuto = QtWidgets.QPushButton(self.stack7)
        self.mainButtonAuto.setGeometry(QtCore.QRect(int(750*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.mainButtonAuto.setIcon(QtGui.QIcon('main.png'))
        self.mainButtonAuto.setIconSize(QSize(35,35))
        self.mainButtonAuto.setObjectName("mainButtonAuto")
        #Set the names of the buttons
        self.retranslateUi_stack_7()

    def retranslateUi_stack_7(self):
        """
        A function that set the names of the buttons
        """
        # self.label3.setText("Communicate with your eyes")
        self.startButtonAuto.setText("")
        self.nextButtonAuto.setText("")
        self.stopButtonAuto.setText("")
        self.mainButtonAuto.setText("")


    def setupUi_stack_8(self):
        """
        This function defines the components of the seventh stack
        This stack contains 4 buttons : 
            *Start Button: start the automatic mode ( implementing the reinforcement learning)
            *Next Button : redirect the user to the next question after he answered a question with YES,
            the assistant press this button after finishing th service chosen by the patient.
            *Stop Button : stop the automatic mode and show the main image (that have "GAZE TRACKER" text).
            *Main Button : redirects the user to the first stack.
        and an image "baseImage_auto" which defines the board in wich we write, questions, choices and notifications.
        """
        self.stack8.resize(self.screen_width, int(680*self.rf_y))
        #Setting the board.
        self.baseImage_auto8 = QtWidgets.QLabel(self.stack8)
        self.baseImage_auto8.setGeometry(QtCore.QRect(0, int(40*self.rf_y), int(1291*self.rf_x), int(640*self.rf_y)))
        self.baseImage_auto8.setFrameShape(QtWidgets.QFrame.Box)
        self.baseImage_auto8.setText("")
        self.baseImage_auto8.setObjectName("baseImage_8")
        #Titre application
        # self.label3 = QtWidgets.QLabel(self.stack7)
        # self.label3.setFont(QtGui.QFont("Titre", 12, QtGui.QFont.Bold))
        # self.label3.setGeometry(1100,10, 300, 200)
        #Start Button
        self.startButtonAuto8 = QtWidgets.QPushButton(self.stack8)
        self.startButtonAuto8.setGeometry(QtCore.QRect(int(450*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.startButtonAuto8.setIcon(QtGui.QIcon('start.png'))
        self.startButtonAuto8.setIconSize(QSize(40,40))
        self.startButtonAuto8.setObjectName("startButtonAuto8")
        #Next Button
        self.nextButtonAuto8 = QtWidgets.QPushButton(self.stack8)
        self.nextButtonAuto8.setGeometry(QtCore.QRect(int(550*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.nextButtonAuto8.setIcon(QtGui.QIcon('Next.png'))
        self.nextButtonAuto8.setIconSize(QSize(40,40))
        self.nextButtonAuto8.setObjectName("nextButtonAuto8")
        #Stop Button
        self.stopButtonAuto8 = QtWidgets.QPushButton(self.stack8)
        self.stopButtonAuto8.setGeometry(QtCore.QRect(int(650*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.stopButtonAuto8.setIcon(QtGui.QIcon('stop.png'))
        self.stopButtonAuto8.setIconSize(QSize(40,40))
        self.stopButtonAuto8.setObjectName("stopButtonAuto8")
        #Main Button
        self.mainButtonAuto8 = QtWidgets.QPushButton(self.stack8)
        self.mainButtonAuto8.setGeometry(QtCore.QRect(int(750*self.rf_x), 0, int(50*self.rf_x), int(40*self.rf_y)))
        self.mainButtonAuto8.setIcon(QtGui.QIcon('main.png'))
        self.mainButtonAuto8.setIconSize(QSize(35,35))
        self.mainButtonAuto8.setObjectName("mainButtonAuto8")
        #Set the names of the buttons
        self.retranslateUi_stack_8()

    def retranslateUi_stack_8(self):
        """
        A function that set the names of the buttons
        """
        # self.label3.setText("Communicate with your eyes")
        self.startButtonAuto8.setText("")
        self.nextButtonAuto8.setText("")
        self.stopButtonAuto8.setText("")
        self.mainButtonAuto8.setText("")