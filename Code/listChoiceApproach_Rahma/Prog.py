"""
Name: Prog.py
Author: Rahma Hellali - written at Higher Institut of Managament,Biware, Tunisia 
Contact: hellalirahma95@gmail.com
Created: 30/05/2019
Updated: 20/12/2019
"""

import numpy as np
import matplotlib.pyplot as plt
import random
import csv
import os.path
from Config import *
from Environment import *
from Classifier import *
from ClassifierSet import *
from MatchSet import *
from random import randint
import Config
from ActionSet import *
import numpy
import numpy.random
from copy import deepcopy
import pandas as pd


os.chdir("C:/Users/RAHMA/listChoiceApproach")

textdata1 = pd.read_csv('8actions_DS.csv')
real_action = textdata1['real_action'].values.tolist()
size_simulation=len(real_action)

textdata2 = pd.read_csv('Test_DS.csv')
real_action_test = textdata2['real_action'].values.tolist()
size_simulation_test=len(real_action_test)

class Prog:
    def __init__(self):
        self.env = Environment()
        self.env_test = Environment_test()
    def init(self,actual_time):
        self.env = Environment()
        self.perf = []
        self.PCC = 0
    def init2(self,actual_time):
        
        self.env_test = Environment_test()
        self.PCC = 0
        self.PCC_test = 0
        textdata = pd.read_csv('population10000.csv')
        
        tmin = textdata['tmin'].values.tolist()
        tmax = textdata['tmax'].values.tolist()
        
        tpmin = textdata['tpmin'].values.tolist()
        tpmax = textdata['tpmax'].values.tolist()
        
        tp_w_min= textdata['tp_weather_min'].values.tolist()
        tp_w_max= textdata['tp_weather_max'].values.tolist()
        
        lmin = textdata['lmin'].values.tolist()
        lmax = textdata['lmax'].values.tolist()
        
        state_window_min = textdata['state_window_min'].values.tolist()
        state_window_max = textdata['state_window_max'].values.tolist()
        
        state_bed_min = textdata['state_bed_min'].values.tolist()
        state_bed_max = textdata['state_bed_max'].values.tolist()
        
        action = textdata['action'].values.tolist()
        fit = textdata['fitness'].values.tolist()
        predict = textdata['prediction'].values.tolist()
        err = textdata['error'].values.tolist()
        num = textdata['numerosity'].values.tolist()
        exper = textdata['experience'].values.tolist()
        timestamp = textdata['time_stamp'].values.tolist()
        actionsetsize = textdata['action_set_size'].values.tolist()     


        i=0
        while len(self.pop.clSet) < len(tmin):
            cond=[tmin[i],tmax[i],tpmin[i],tpmax[i],tp_w_min[i],tp_w_max[i],lmin[i],lmax[i],state_window_min[i],state_window_max[i],state_bed_min[i],state_bed_max[i]]
            clm = Classifier(cond,actual_time)
            clm.condition = cond
            clm.action = action[i]
            clm.time_stamp = timestamp[i]
            clm.numerosity = num[i]
            clm.action_set_size = actionsetsize[i]
            clm.prediction = predict[i]
            clm.error = err[i]
            clm.fitness = fit[i]
            clm.experience = exper[i]
            self.pop.clSet.append(clm)
            i+=1


    def run_learning(self):
        # main function
        self.actual_time = 0.0
        self.pop = ClassifierSet(self.env,self.actual_time)
        self.init(self.actual_time)
        self.list_ranks_action_found = []
        self.update_iteration_number = 25
        self.threshold = conf.Number_of_actions
        self.attempt_list = []

        j=0
        while j <= (conf.max_iterations)-1:
            for iteration in range(size_simulation):          

                if j % self.update_iteration_number == 0 and j!=0:
                    #calcul moyenne des rang
                    moy = sum(self.list_ranks_action_found)/len(self.list_ranks_action_found)

                    if moy < self.threshold:
                        self.threshold -= 1
                    else:
                        self.threshold += 1
                    #clear the list after every update 
                    self.list_ranks_action_found.clear()
                self.run_XCSR(iteration,self.threshold)
#                print("iteration number:   "+str(j))
                j=j+1
                
                if j >= conf.max_iterations :
                    break        

        PCC_total=(self.PCC/(conf.max_iterations))*100
        print("Number of correct classifier :    "+str(self.PCC)+"/"+str(conf.max_iterations))
        print("Pourcentage of correct classifier (PCC) :   "+str(round(PCC_total,4))+" %")
        file_name=conf.max_iterations
        self.file_writer(file_name)
        self.attempt_writer(file_name)


    def run_test(self):
        # main function
        self.actual_time = 0.0
        self.pop = ClassifierSet(self.env_test,self.actual_time)
        self.init2(self.actual_time)
        self.list_ranks_action_found = []
        self.update_iteration_number = 25
        self.threshold = conf.Number_of_actions
        self.attempt_list = []
            
        for iteration in range(size_simulation_test):
            if iteration % self.update_iteration_number == 0 and iteration!=0:
                    #calcul moyenne des rang
                moy = sum(self.list_ranks_action_found)/len(self.list_ranks_action_found)
                if moy < self.threshold:
                    self.threshold -= 1
                else:
                    self.threshold += 1
                    #clear the list after every update 
                self.list_ranks_action_found.clear()
            self.run_evaluation(iteration,self.threshold)
#            print("iteration number:   " + str(iteration))
       
        PCC_total=(self.PCC/(size_simulation_test))*100
        print("Number of correct classifier :    "+str(self.PCC)+"/"+str(size_simulation_test))
        print("Pourcentage of correct classifier (PCC) :   "+str(round(PCC_total,4))+" %")
        file_name = size_simulation_test
        self.file_writer(file_name)
        self.attempt_Writer_Test(file_name)
        self.Pourcentage_attempt_calculation(file_name)
        self.graph(file_name)

    def run_XCSR(self,i,rank):

        self.env.set_state(i)
        
        self.match_set = MatchSet(self.pop,self.env,self.actual_time)

        self.generate_prediction_array()
       
        self.List_choice_select_action(i,rank)
        
        #every iteration we record the iteration rank in which we found the correct answer
        self.list_ranks_action_found.append(self.rank_find)      

        self.action_set = ActionSet(self.match_set,self.action,self.env,self.actual_time)

        self.action_set.do_action(i)

        self.action_set.update_action_set()

        self.action_set.do_action_set_subsumption(self.pop)

        self.run_GA()

        if len(self.pop.clSet) > conf.Max_size_population:
            self.pop.delete_from_population()
        self.actual_time += 1.0

    def run_evaluation(self,i,rank):

        self.env_test.set_state(i)
        
        self.match_set = MatchSet(self.pop,self.env_test,self.actual_time)

        self.generate_prediction_array()

        self.List_choice_select_action(i,rank)
        
        #every iteration we record the iteration rank in which we found the correct answer
        self.list_ranks_action_found.append(self.rank_find)   

        self.action_set = ActionSet(self.match_set,self.action,self.env_test,self.actual_time)

        self.action_set.do_action(i)

        self.action_set.update_action_set()

        self.action_set.do_action_set_subsumption(self.pop)

        self.run_GA()

        if len(self.pop.clSet) > conf.Max_size_population:
            self.pop.delete_from_population()
        self.actual_time += 1.0


    def List_choice_select_action(self,i,rank):
        list_rank=self.get_list_with_rank(rank)
        self.action = None
        self.rank_find = None
        l=0
        while l < len(list_rank):

            if self.env.is_true(list_rank[l],i)== True:
                self.action = list_rank[l]
                self.rank_find= l+1
                break
            l+=1

        if self.action == None and self.rank_find == None:
            self.action = random.choice(self.get_rest_list_action(list_rank))
            self.rank_find = len(list_rank)+1

        self.attempt_list.append(self.rank_find)
        
        if self.rank_find == 1 or self.rank_find == 2:
            self.PCC+=1

    def best_action(self):
        """ Selects the action in the prediction array with the best value.
         *MODIFIED so that in the case of a tie between actions - an action is selected randomly between the tied highest actions. """
        big = self.p_array[0]
        best = 0
        for i in range(Config.Actions_count):
            if big < self.p_array[i]:
                big = self.p_array[i]
                best = i
        return best

    def get_list_with_rank(self,rank):
        l = sorted( [(x,i) for (i,x) in enumerate(self.p_array)], reverse=True )[:rank]
        self.values = []
        self.posns = []
        for x,i in l:
            self.values.append( x )
            self.posns.append( i )
        return self.posns

    def get_list_actions(self):
        list_actions = []
        for i in range(conf.Number_of_actions):
            list_actions.append(i)
        return list_actions

    def get_rest_list_action(self,list_top):
        
        list_actions=self.get_list_actions()
        list_reste = []
        for i in range(len(list_actions)):
            if list_actions[i] not in list_top:
                list_reste.append(list_actions[i])
        return list_reste

    def generate_prediction_array(self):
        self.p_array = [0] * conf.Number_of_actions
        self.f_array = [0] * conf.Number_of_actions
        for cl in self.match_set.clSet:
            self.p_array[cl.action] += cl.prediction*cl.fitness
            self.f_array[cl.action] += cl.fitness
        for i in range(conf.Number_of_actions):
            if self.f_array[i] != 0:
                self.p_array[i] /= self.f_array[i]

    def select_offspring(self):
        """ Selects one classifier using roulette wheel selection according to the fitness of the classifiers. """
        fit_sum=0
        for q in self.action_set.clSet:
            fit_sum = fit_sum + q.fitness
        choice_point = fit_sum * random.random()
        fit_sum = 0.0
        for cl in self.action_set.clSet:
            fit_sum = fit_sum + cl.fitness
            if fit_sum > choice_point:
                return cl
        return None

    def twoPointCrossover(self,cl1,cl2):
        """ Applies two point crossover and returns if the classifiers changed. """

        length = len(cl1.condition)
        sep1 = int(random.random()*(length))
        sep2 = int(random.random()*(length))
        if sep1>sep2:
            sep1,sep2 = sep2,sep1
        elif sep1==sep2:
            sep2 = sep2+1
        cond1 = cl1.condition
        cond2 = cl2.condition

        for i in range(sep1,sep2):
            if cond1[i] != cond2[i]:
                cond1[i],cond2[i] = cond2[i],cond1[i]
        cl1.condition = cond1
        cl2.condition = cond2



    def add_Time(self,time,time_plus):
        time=str(time)
        minute_part = time[3] + time[4]
        hour_part = time[0] + time[1]
        minute = int(minute_part) + time_plus
        if minute >= 60:
            minute = minute-60
            hour = int(hour_part)+1
        else:
            hour = int(hour_part)
        if hour < 10:
            hour = "0" + str(hour)
        if minute < 10:
            minute = "0" + str(minute)
        Tmax = str(hour) + ":" + str(minute)
        return (Tmax)


    def add_Number(self,val,rand_num):
        new_val= val + rand_num
        return new_val

    def ajouter_valeur_aleatoire(self,attr1,attr2):
        if isinstance(attr1,str) == True and isinstance(attr2, str) == True:
            #ajouter une valeur aleatoire pour le temps d'une val random de 15 minutes
            rand_temps=randint(0,15)
            at1 = self.add_Time(attr1,rand_temps)
            at2 = self.add_Time(attr2,rand_temps)
        else:
            #ajouter pour les autres variables valeur = +/- 2
            rand_number=randint(0,2)
            at1 = self.add_Number(attr1,rand_number)
            at2 = self.add_Number(attr2,rand_number)
        return at1,at2

    def apply_mutation(self,cl):
        """ Applies a niche mutation to the classifier.
            Since we work with intervals, we choose an interval randomly and we add a random number between $(0, rand)$, $rand$ a constant number set from the beginning (as illustrated in Figure \ref{Mutation}). 
            In this way, we ensure that we obtain diversification and have a feasible condition.
            We have chosen rand = 15 minutes for the time, for the indoor and outdoor temperature rand = 2 degrees, for the luminosity rand = 5 and the window and the bed state rand = 0.3. The mutation also occurs at the action part, we choose a random action among the available actions."""
        if random.random() < conf.proba_mutation:
            #mutation pour la condition
            if len(cl.condition) % 2 == 0:
                number_interval = (len(cl.condition)/2)-1
                rand_interval = randint(0,number_interval)
                indice_borne_inf = rand_interval*2
                indice_borne_sup = (rand_interval*2) + 1
                e1, e2 = self.ajouter_valeur_aleatoire(cl.condition[indice_borne_inf],cl.condition[indice_borne_sup])
                cl.condition[indice_borne_inf] = e1
                cl.condition[indice_borne_sup] = e2

        if random.random() < conf.proba_mutation:
            #mutation pour les action
            cl.action = random.randrange(conf.Number_of_actions)
    def run_GA(self):
        """ The Genetic Discovery in XCS takes place here. If a GA takes place, two classifiers are selected
        by roulette wheel selection, possibly crossed and mutated and then inserted."""
        if len(self.action_set.clSet)==0:
            return
        if self.actual_time - self.action_set.ts_num_sum()/self.action_set.numerosity_sum() > conf.theta_ga:
            for cl in self.action_set.clSet:
                cl.time_stamp = self.actual_time

            parent_1 = self.select_offspring()
            parent_2 = self.select_offspring()
            child_1 = parent_1.deep_copy(self.actual_time)
            child_2 = parent_2.deep_copy(self.actual_time)
            child_1.numerosity = 1
            child_2.numerosity = 1
            child_1.experience = 0
            child_2.experience = 0
            if random.random() < conf.proba_crossover:
                self.twoPointCrossover(child_1,child_2)
                child_1.prediction = (parent_1.prediction+parent_2.prediction)/2.0
                child_1.error = 0.25*(parent_1.error+parent_2.error)/2.0
#                child_1.error = (parent_1.error+parent_2.error)/2.0
                child_1.fitness = 0.1*(parent_1.fitness+parent_2.fitness)/2.0
#                child_1.fitness = (parent_1.fitness+parent_2.fitness)/2.0
                child_2.prediction = child_1.prediction
                child_2.error = child_1.error
                child_2.fitness = child_1.fitness
            self.apply_mutation(child_1)
            self.apply_mutation(child_2)
            if conf.doGASubsumption:
                if parent_1.does_subsume(child_1):
                    parent_1.numerosity += 1
                elif parent_2.does_subsume(child_1):
                    parent_2.numerosity += 1
                else:
                    self.pop.insert_in_population(child_1)
                if parent_1.does_subsume(child_2):
                    parent_1.numerosity += 1
                elif parent_2.does_subsume(child_2):
                    parent_2.numerosity += 1
                else:
                    self.pop.insert_in_population(child_2)
            else:
                self.pop.insert_in_population(child_1)
                self.pop.insert_in_population(child_2)
            while self.pop.numerosity_sum() > conf.Max_size_population:
                self.pop.delete_from_population()

    def file_writer(self,num):
        file_name = "population"+str(num)+".csv"
        write_csv = csv.writer(open(file_name,'w'),lineterminator='\n')
        write_csv.writerow(["tmin","tmax","tpmin","tpmax","tp_weather_min","tp_weather_max","lmin","lmax","state_window_min","state_window_max","state_bed_min","state_bed_max","action","fitness","prediction","error","numerosity","experience","time_stamp","action_set_size"])
        for cl in self.pop.clSet:

            cond = ""
            for c in cl.condition:
                cond+=str(c)+"-"
                tmin=cl.condition[0]
                tmax=cl.condition[1]
                tpmin=cl.condition[2]
                tpmax=cl.condition[3]
                tp_weather_min=cl.condition[4]
                tp_weather_max=cl.condition[5]
                lmin=cl.condition[6]
                lmax=cl.condition[7]
                state_window_min=cl.condition[8]
                state_window_max=cl.condition[9]
                state_bed_min=cl.condition[10]
                state_bed_max=cl.condition[11]
            write_csv.writerow([tmin,tmax,tpmin,tpmax,tp_weather_min,tp_weather_max,lmin,lmax,state_window_min,state_window_max,state_bed_min,state_bed_max,cl.action,cl.fitness,cl.prediction,cl.error,cl.numerosity,cl.experience,cl.time_stamp,cl.action_set_size])


    def attempt_writer(self,num):
        file_name = "attempt_list"+str(num)+".csv"
        write_csv = csv.writer(open(file_name,'w'),lineterminator='\n')
        write_csv.writerow(["iteration","attempt_number"])
        for k in range(len(self.attempt_list)):
            write_csv.writerow([k,self.attempt_list[k]])
    
    
    def attempt_Writer_Test(self,num):
        file_name = "attempt_list_test"+str(num)+".csv"
        write_csv = csv.writer(open(file_name,'w'),lineterminator='\n')
        write_csv.writerow(["iteration","attempt_number"])
        for k in range(len(self.attempt_list)):
            write_csv.writerow([k,self.attempt_list[k]])

    def graph(self,num):
        #for each iteration, we record the number of the attempt in which the system found the correct action
        textdata3 = pd.read_csv('attempt_list_test'+str(num)+'.csv')
        iteration = textdata3['iteration'].values.tolist()
        attempt_number = textdata3['attempt_number'].values.tolist()
        l=[]
        m=[]
        for i in range(len(iteration)):
            if i%1==0:
                if attempt_number[i]!=6:
                    l.append(i)
                    m.append(attempt_number[i])

        fig = plt.figure(figsize=(16, 10))
        ax = fig.add_subplot(1,1,1)  
        x = np.array(l)
        y = np.array(m)
        plt.plot(x,y)
        plt.xlabel('Iterations') 
        plt.ylabel('attempt_number')
        plt.show()
        
    def graph2(self,num):
        textdata3 = pd.read_csv('attempt_list'+str(num)+'.csv')
        iteration = textdata3['iteration'].values.tolist()
        name = '1', '2', '3', '4','5','6','7','8'
        attempt_number = textdata3['attempt_number'].values.tolist()
        l=[]
        
        for j in range(8):
            summ=0
            for n in range(len(attempt_number)):
                if attempt_number[n]==j+1:
                    summ+=1
            l.append(summ)
        explode=(0.15, 0, 0, 0, 0, 0, 0, 0)
        fig = plt.figure(figsize=(16, 10))
        ax = fig.add_subplot(1,1,1)  
        plt.pie(l, explode=explode, labels=name, autopct='%1.1f%%', startangle=90, shadow=True)
        plt.axis('equal')
        plt.show()
        
    def Pourcentage_attempt_calculation(self,num):
        textdata3 = pd.read_csv('attempt_list_test'+str(num)+'.csv')
        iteration = textdata3['iteration'].values.tolist()
        attempt_number = textdata3['attempt_number'].values.tolist()
        l=[]
        for j in range(8):
            summ=0
            for n in range(len(attempt_number)):
                if attempt_number[n]==j+1:
                    summ+=1
            l.append(summ)
        for e in range(len(l)):
            print("Percentage of correct classifiers found on " +str(e+1) +" the attempt : " + str(round(l[e]*100/size_simulation_test,2))+" %")


if __name__ == '__main__':
    xcs=Prog()
    print("***************** knowledge acquisition ******************")
    print("Data set size :   " +str(size_simulation))
    print("Iteration number :   " +str(conf.max_iterations))
    print("Max size population :   " +str(conf.Max_size_population))
    xcs.run_learning()
    print("***************** Evaluation part ******************")
    print("Data set size :   " +str(size_simulation_test))
    xcs.run_test()
