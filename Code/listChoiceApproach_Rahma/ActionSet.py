import random
from Config import *
from Environment import *
from Classifier import *
from ClassifierSet import *
from MatchSet import *
from random import randint

class ActionSet(ClassifierSet):
    def __init__(self,match_set,action,state,actual_time):
        ClassifierSet.__init__(self,state,actual_time)
        self.action = action
        for cl in match_set.clSet:
            if cl.action == self.action:
                self.clSet.append(cl)
    def do_action(self,i): #simulation
        if self.env.is_true(self.action,i):
            self.reward = 1000
#            print("positif reward :  "+str(self.reward))
        else:
            self.reward = 0
#            print("reward null :  "+str(self.reward))
#        return self.reward
    def get_do_action(self):
        return self.reward


    def update_action_set(self):
        num_sum = self.numerosity_sum()
        for cl in self.clSet:
            cl.update_parameters(self.reward,num_sum)
        acc_sum = self.accuracy_sum()
        for cl in self.clSet:
            cl.update_fitness(acc_sum)
    """
        Does subsumption inside the action set, finding the most general 
        classifier and merging things into it
    """
    def do_action_set_subsumption(self,pop):
        """ Executes action set subsumption.  The action set subsumption looks for the most general subsumer classifier in the action set
        and subsumes all classifiers that are more specific than the selected one. """
        if conf.doActionSetSubsumtion:
            subsumer = None
            for cl in self.clSet:
                if cl.could_subsume():
                    if subsumer==None or cl.is_more_general(subsumer):
                        subsumer = cl
            if subsumer!=None:
                i=0
                while i<len(self.clSet):
                    if subsumer.is_more_general(self.clSet[i]):
                        subsumer.numerosity += self.clSet[i].numerosity
                        pop.remove_classifier_by_instance(self.clSet[i])
                        self.remove_classifier(i)
                        i -= 1
                    i += 1


# for debug
#if __name__ == '__main__':
##    random.seed(3)
#    env = Environment()
#    env.set_state()
#    x = ClassifierSet(env,1)
#    y = MatchSet(x,env,1)
#    print (env.state)
#    for cl in y.cls:
#        print ("condition:   "+str(cl.condition))
#        print("action:   "+str(cl.action))
#        print("prediction:   "+str(cl.prediction ))
#        print("error:   "+str(cl.error ))
#        print("fitness:   "+str(cl.fitness ))
#        print("numerosity:   "+str(cl.numerosity ))
#        print("experience:   "+str(cl.experience ))
#        print("time stamp:   "+str(cl.time_stamp ))
#        print("action_set_size:   "+str(cl.action_set_size ))
#    print ("\n")
#    a = ActionSet(y,16,env,1)
#    a.do_action()
#    a.update_action_set()
#    for cl in a.cls:        
#        print ("condition:   "+str(cl.condition))
#        print("action:   "+str(cl.action))
#        print("prediction:   "+str(cl.prediction ))
#        print("error:   "+str(cl.error ))
#        print("fitness:   "+str(cl.fitness ))
#        print("numerosity:   "+str(cl.numerosity ))
#        print("experience:   "+str(cl.experience ))
#        print("time stamp:   "+str(cl.time_stamp ))
#        print("action_set_size:   "+str(cl.action_set_size ))

