import pandas as pd
import os


"""************************for testing*******************"""

os.chdir("C:/Users/RAHMA/listChoiceApproach")
textdata1 = pd.read_csv('Test_DS.csv')
Time = textdata1['Time'].values.tolist()
Temperature_Comedor_Sensor = textdata1['Temperature_Comedor_Sensor'].values.tolist()
Temperature_Exterior_Sensor = textdata1['Temperature_Exterior_Sensor'].values.tolist()
Lighting_Habitacion_Sensor = textdata1['Lighting_Habitacion_Sensor'].values.tolist()
state_window = textdata1['state_window'].values.tolist()
state_bed = textdata1['state_bed'].values.tolist()
real_action = textdata1['real_action'].values.tolist()
size_simulation=len(real_action)


class Environment_test:


    def set_state(self,i):
        self.__state=[]
        time_state = Time[i]
        room_temperature_state = int(Temperature_Comedor_Sensor[i])
        exterior_temperature_state = int(Temperature_Exterior_Sensor[i])
        Room_lighting_state = int(Lighting_Habitacion_Sensor[i])
        window_state = round(state_window[i],2)
        bed_state = round(state_bed[i],2)
        self.__state.append(time_state)
        self.__state.append(room_temperature_state)
        self.__state.append(exterior_temperature_state)
        self.__state.append(Room_lighting_state)
        self.__state.append(window_state)
        self.__state.append(bed_state)

    def get_state(self):
        return self.__state
    state = property(get_state)

    def is_true(self,proposed_action,i):
        if proposed_action== real_action[i]:
            return True
        else:
            return False
