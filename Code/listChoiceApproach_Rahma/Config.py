"""
Name: Prog.py
Author: Rahma Hellali - written at Higher Institut of Managament,Biware, Tunisia 
Contact: hellalirahma95@gmail.com
Created: 30/05/2019
Updated: 20/12/2019
Description: Specifies default XCS run parameters.  Most parameter-names are chosen similar to * the 'An Algorithmic Description of XCS' 
(Butz&Wilson, IlliGAL report 2000017)
"""
import pandas as pd
import os


os.chdir("C:/Users/RAHMA/listChoiceApproach")

textdata = pd.read_csv('8actions.csv')
actions_list = textdata['action'].values.tolist()
num_action = textdata['num_action'].values.tolist()
Actions_count = len(num_action)

class Config:
    """ Specifies default XCS run constants. """
    Number_of_actions = Actions_count

    Max_size_population = 6000
    max_iterations = 10000


    alpha = 0.1                # The fall of rate in the fitness evaluation.
    beta = 0.2                 # The learning rate for updating fitness, prediction, prediction error, and action set size estimate in XCS's classifiers.
    gamma = 0.71               # The discount rate in multi-step problems. here our problem is signle step
    delta = 0.1                # The fraction of the mean fitness of the population below which the fitness of a classifier may be considered in its vote for deletion.
    proba_mutation = 0.01      # The probability of mutating one allele and the action in an offspring classifier.
    nyu = 5                    # Specifies the exponent in the power function for the fitness evaluation.
    proba_crossover = 0.5       # The probability of applying crossover in an offspring classifier.

    epsilon_0 = 10             #1% reward here we choose to specify the reward as 1000 
                                # The error threshold under which the accuracy of a classifier is set to one.

    theta_ga = 50  # The threshold for the GA application in an action set. # la marge est de 25 à 50
    theta_del = 20 # Specified the threshold over which the fitness of a classifier may be considered in its deletion probability.
    theta_sub = 20
    min_number_action_in_MatchSet = Actions_count

    p_sharp = 0.33
    #exploration_probability
    p_explr= 1.0

    doGASubsumption = True
    doActionSetSubsumtion = True

XCSConfig = Config()
conf = XCSConfig #To access one of the above constant values from another module, import Config * and use "cons.Xconstant"
