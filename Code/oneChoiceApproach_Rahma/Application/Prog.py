"""
Name: Prog.py
Author: Rahma Hellali - written at Higher Institut of Managament,Biware, Tunisia 
Contact: hellalirahma95@gmail.com
Created: 30/05/2019
Updated: 20/12/2019
"""
import numpy as np
import matplotlib.pyplot as plt
import random
import csv
import os.path
from Config import *
from Environment import *
from Classifier import *
from ClassifierSet import *
from MatchSet import *
from random import randint
import Config
from ActionSet import *
import numpy
import numpy.random
from copy import deepcopy
import pandas as pd


os.chdir("C:/Users/RAHMA/oneChoiceApproach/Application")

textdata1 = pd.read_csv('8actions_DS.csv')
real_action = textdata1['real_action'].values.tolist()
size_simulation=len(real_action)

textdata2 = pd.read_csv('Test2.csv')
real_action_test = textdata2['real_action'].values.tolist()
size_simulation_test=len(real_action_test)

class Prog:
    def __init__(self):
        self.env = Environment()
        self.env_test = Environment_test()
        self.perf = []
    def init(self,actual_time):
        self.env = Environment()
        self.perf = []
        self.PCC = 0
    def init2(self,actual_time):
        
        self.env_test = Environment_test()
        self.PCC_test = 0
        textdata = pd.read_csv('population10000.csv')
        
        tmin = textdata['tmin'].values.tolist()
        tmax = textdata['tmax'].values.tolist()
        
        tpmin = textdata['tpmin'].values.tolist()
        tpmax = textdata['tpmax'].values.tolist()
        
        tp_w_min= textdata['tp_weather_min'].values.tolist()
        tp_w_max= textdata['tp_weather_max'].values.tolist()
        
        lmin = textdata['lmin'].values.tolist()
        lmax = textdata['lmax'].values.tolist()
        
        state_window_min = textdata['state_window_min'].values.tolist()
        state_window_max = textdata['state_window_max'].values.tolist()
        
        state_bed_min = textdata['state_bed_min'].values.tolist()
        state_bed_max = textdata['state_bed_max'].values.tolist()
        
        action = textdata['action'].values.tolist()
        fit = textdata['fitness'].values.tolist()
        predict = textdata['prediction'].values.tolist()
        err = textdata['error'].values.tolist()
        num = textdata['numerosity'].values.tolist()
        exper = textdata['experience'].values.tolist()
        timestamp = textdata['time_stamp'].values.tolist()
        actionsetsize = textdata['action_set_size'].values.tolist()     


        i=0
        while len(self.pop.clSet) < len(tmin):
            cond=[tmin[i],tmax[i],tpmin[i],tpmax[i],tp_w_min[i],tp_w_max[i],lmin[i],lmax[i],state_window_min[i],state_window_max[i],state_bed_min[i],state_bed_max[i]]
            clm = Classifier(cond,actual_time)
            clm.condition = cond
            clm.action = action[i]
            clm.time_stamp = timestamp[i]
            clm.numerosity = num[i]
            clm.action_set_size = actionsetsize[i]
            clm.prediction = predict[i]
            clm.error = err[i]
            clm.fitness = fit[i]
            clm.experience = exper[i]
            self.pop.clSet.append(clm)
            i+=1


    def run_learning(self):
        for exp in range(1):
            
            # main function
            self.actual_time = 0.0
            self.pop = ClassifierSet(self.env,self.actual_time)
            self.init(self.actual_time)
            
            j=0
            while j <= (conf.max_iterations)-1:
                for iteration in range(size_simulation):
                    self.run_explor(iteration)
                    self.run_exploit(iteration,j)
    #                print("iteration number:   " + str(j))
    #                if j == 1800 :
    #                    conf.p_explr = 0.5
                    if j == 2000:
                        conf.p_explr = 0.5
                    if j == 6000 :
                        conf.p_explr = 0.001
                    j=j+1
                    if j >= conf.max_iterations-1 :
                        break        
            PCC_total=(self.PCC/(conf.max_iterations))*100
            print("nombre des réponses correctes:    "+str(self.PCC)+"/"+str(conf.max_iterations))
            print("PCC :   "+str(round(PCC_total,4))+" %")
            file_name=conf.max_iterations
            self.file_writer(file_name)
            #self.performance_writer(10)
        #self.make_graph()
            
            
    def run_exploit(self,i,j):
        if j%100==0:
            p = 0
            for i in range(100):
                self.env.set_state(i)
                self.match_set = MatchSet(self.pop,self.env,self.actual_time)
                self.generate_prediction_array()
                self.action = self.best_action()
                if self.env.is_true(self.action,i):
                    p += 1
            self.perf.append(p)

    def run_test(self):
        # main function
        self.actual_time = 0.0
        self.pop = ClassifierSet(self.env_test,self.actual_time)
        self.init2(self.actual_time)
            
        for iteration in range(size_simulation_test):
            self.run_explor_test(iteration)
#            print("iteration number:   " + str(iteration))
       
        PCC_total=(self.PCC_test/(size_simulation_test))*100
        print("nombre des réponses correctes:    "+str(self.PCC_test)+"/"+str(size_simulation_test))
        print("PCC :   "+str(round(PCC_total,4))+" %")
        file_name = size_simulation_test
        self.file_writer(file_name)

    def run_explor(self,i):

        self.env.set_state(i)
        
        self.match_set = MatchSet(self.pop,self.env,self.actual_time)

        self.generate_prediction_array()

        self.select_action()

        self.action_set = ActionSet(self.match_set,self.action,self.env,self.actual_time)

        self.action_set.do_action(i)

        if self.action_set.get_do_action()==1000:
            self.PCC+=1

        self.action_set.update_action_set()

        self.action_set.do_action_set_subsumption(self.pop)

        self.run_GA()

        if len(self.pop.clSet) > conf.Max_size_population:
            self.pop.delete_from_population()
        self.actual_time += 1.0
#        if i==0:
#            print(self.env.state)
#            print("selected action:    " + Config.actions_list[self.action])

    def run_explor_test(self,i):

        self.env_test.set_state(i)
        
        self.match_set = MatchSet(self.pop,self.env_test,self.actual_time)

        self.generate_prediction_array()

        self.select_action_test()

        self.action_set = ActionSet(self.match_set,self.action,self.env_test,self.actual_time)

        self.action_set.do_action(i)

        if self.action_set.get_do_action()==1000:
            self.PCC_test += 1

        self.action_set.update_action_set()

        self.action_set.do_action_set_subsumption(self.pop)

        self.run_GA()

        if len(self.pop.clSet) > conf.Max_size_population:
            self.pop.delete_from_population()
        self.actual_time += 1.0



    def select_action(self):
        if random.random() > conf.p_explr:
            self.action = self.best_action()
        else:
            self.action = random.randrange(Config.Actions_count)
#        print("Action selection :    "+str(Config.actions_list[self.action]))

    def select_action_test(self):
            self.action = self.best_action()

    def best_action(self):
        big = self.p_array[0]
        best = 0
        for i in range(Config.Actions_count):
            if big < self.p_array[i]:
                big = self.p_array[i]
                best = i
        return best


    def generate_prediction_array(self):
        self.p_array = [0] * conf.Number_of_actions
        self.f_array = [0] * conf.Number_of_actions
        for cl in self.match_set.clSet:
            self.p_array[cl.action] += cl.prediction*cl.fitness
            self.f_array[cl.action] += cl.fitness
        for i in range(conf.Number_of_actions):
            if self.f_array[i] != 0:
                self.p_array[i] /= self.f_array[i]

    def select_offspring(self):
        fit_sum=0
        for q in self.action_set.clSet:
            fit_sum = fit_sum + q.fitness
        choice_point = fit_sum * random.random()
        fit_sum = 0.0
        for cl in self.action_set.clSet:
            fit_sum = fit_sum + cl.fitness
            if fit_sum > choice_point:
                return cl
        return None

    def twoPointCrossover(self,cl1,cl2):

        length = len(cl1.condition)
        sep1 = int(random.random()*(length))
        sep2 = int(random.random()*(length))
        if sep1>sep2:
            sep1,sep2 = sep2,sep1
        elif sep1==sep2:
            sep2 = sep2+1
        cond1 = cl1.condition
        cond2 = cl2.condition

        for i in range(sep1,sep2):
            if cond1[i] != cond2[i]:
                cond1[i],cond2[i] = cond2[i],cond1[i]
        cl1.condition = cond1
        cl2.condition = cond2


    def ajouter_temps(self,time,time_plus):
        time=str(time)
        minute_part = time[3] + time[4]
        hour_part = time[0] + time[1]
        minute = int(minute_part) + time_plus
        if minute >= 60:
            minute = minute-60
            hour = int(hour_part)+1
        else:
            hour = int(hour_part)
        if hour < 10:
            hour = "0" + str(hour)
        if minute < 10:
            minute = "0" + str(minute)
        Tmax = str(hour) + ":" + str(minute)
        return (Tmax)


    def ajouter_number(self,val,rand_num):
        new_val= val + rand_num
        return new_val

    def ajouter_valeur_aleatoire(self,attr1,attr2):
        if isinstance(attr1,str)==True and isinstance(attr2, str)==True:
            #ajouter une valeur aleatoire pour le temps d'une val random de 15 minutes
            rand_temps=randint(0,15)
            at1 = self.ajouter_temps(attr1,rand_temps)
            at2 = self.ajouter_temps(attr2,rand_temps)
        else:
            #ajouter pour les autres variables valeur = +/- 4
            rand_number=randint(0,4)
            at1 = self.ajouter_number(attr1,rand_number)
            at2 = self.ajouter_number(attr2,rand_number)
        return at1,at2

    def apply_mutation(self,cl):
        if random.random() < conf.proba_mutation:
            #mutation pour la condition
            if len(cl.condition) % 2 == 0:
                number_interval = (len(cl.condition)/2)-1
                rand_interval = randint(0,number_interval)
                indice_borne_inf = rand_interval*2
                indice_borne_sup = (rand_interval*2) + 1
                e1, e2 = self.ajouter_valeur_aleatoire(cl.condition[indice_borne_inf],cl.condition[indice_borne_sup])
                cl.condition[indice_borne_inf] = e1
                cl.condition[indice_borne_sup] = e2

        if random.random() < conf.proba_mutation:
            #mutation pour les actions
            cl.action = random.randrange(conf.Number_of_actions)


    def run_GA(self):
        if len(self.action_set.clSet)==0:
            return
        if self.actual_time - self.action_set.ts_num_sum()/self.action_set.numerosity_sum()>conf.theta_ga:
            for cl in self.action_set.clSet:
                cl.time_stamp = self.actual_time

            parent_1 = self.select_offspring()
            parent_2 = self.select_offspring()
            child_1 = parent_1.deep_copy(self.actual_time)
            child_2 = parent_2.deep_copy(self.actual_time)
            child_1.numerosity = 1
            child_2.numerosity = 1
            child_1.experience = 0
            child_2.experience = 0
            if random.random() < conf.proba_crossover:
                self.twoPointCrossover(child_1,child_2)
                child_1.prediction = (parent_1.prediction+parent_2.prediction)/2.0
                child_1.error = 0.25*(parent_1.error+parent_2.error)/2.0
#                child_1.error = (parent_1.error+parent_2.error)/2.0
                child_1.fitness = 0.1*(parent_1.fitness+parent_2.fitness)/2.0
#                child_1.fitness = (parent_1.fitness+parent_2.fitness)/2.0
                child_2.prediction = child_1.prediction
                child_2.error = child_1.error
                child_2.fitness = child_1.fitness
            self.apply_mutation(child_1)
            self.apply_mutation(child_2)
            if conf.doGASubsumption:
                if parent_1.does_subsume(child_1):
                    parent_1.numerosity += 1
                elif parent_2.does_subsume(child_1):
                    parent_2.numerosity += 1
                else:
                    self.pop.insert_in_population(child_1)
                if parent_1.does_subsume(child_2):
                    parent_1.numerosity += 1
                elif parent_2.does_subsume(child_2):
                    parent_2.numerosity += 1
                else:
                    self.pop.insert_in_population(child_2)
            else:
                self.pop.insert_in_population(child_1)
                self.pop.insert_in_population(child_2)
            while self.pop.numerosity_sum() > conf.Max_size_population:
                self.pop.delete_from_population()



    def file_writer(self,num):
        file_name = "population"+str(num)+".csv"
        write_csv = csv.writer(open(file_name,'w'),lineterminator='\n')
        write_csv.writerow(["tmin","tmax","tpmin","tpmax","tp_weather_min","tp_weather_max","lmin","lmax","state_window_min","state_window_max","state_bed_min","state_bed_max","action","fitness","prediction","error","numerosity","experience","time_stamp","action_set_size"])
        for cl in self.pop.clSet:

            cond = ""
            for c in cl.condition:
                cond+=str(c)+"-"
                tmin=cl.condition[0]
                tmax=cl.condition[1]
                tpmin=cl.condition[2]
                tpmax=cl.condition[3]
                tp_weather_min=cl.condition[4]
                tp_weather_max=cl.condition[5]
                lmin=cl.condition[6]
                lmax=cl.condition[7]
                state_window_min=cl.condition[8]
                state_window_max=cl.condition[9]
                state_bed_min=cl.condition[10]
                state_bed_max=cl.condition[11]
            write_csv.writerow([tmin,tmax,tpmin,tpmax,tp_weather_min,tp_weather_max,lmin,lmax,state_window_min,state_window_max,state_bed_min,state_bed_max,cl.action,cl.fitness,cl.prediction,cl.error,cl.numerosity,cl.experience,cl.time_stamp,cl.action_set_size])


    def performance_writer(self,num):
        file_name = "performance" + str(num) + ".csv"
        np.savetxt(file_name, np.array(self.perf),fmt="%d", delimiter=",")
    def make_graph(self):
        performance = []
        
        i = 0
        file_path = "performance" + str(i) + ".csv"
        while os.path.exists(file_path):
            pf = np.loadtxt(file_path,delimiter=",")
            performance.append(pf)
            i += 1
            file_path = "performance" + str(i) + ".csv"
        
        data_num = i
        
        data_length = len(np.loadtxt("performance0.csv",delimiter=","))
        pf = []
        
        x = np.arange(0,data_length*100,100)
        for i in range(data_length):
            sum = 0.0
            for j in range(data_num):
                sum += performance[j][i]
            pf.append(sum/float(data_num))
        pf = np.array(pf)
        np.savetxt("ave_performance.csv",pf,delimiter=",")
        fig = plt.figure(figsize=(16, 10))
        ax = fig.add_subplot(1,1,1)
        ax.plot(x, pf, linewidth=2, label='performance')
        ax.set_ylim(40, 110)
        ax.set_xlim(0, data_length*100)
        ax.set_title('Performance')
        ax.set_yticklabels(['40%','50%','60%','70%','80%','90%','100%',''])
        ax.grid()
        filenamepng = "performance.png"
        plt.savefig(filenamepng, dpi=150)
        filenameeps = "performance.eps"
        plt.savefig(filenameeps)
        plt.show()




if __name__ == '__main__':
    xcs=Prog()
    print("***************** knowledge acquisition ******************")
    print("Data set size :   " +str(size_simulation))
    print("Iteration number :   " +str(conf.max_iterations))
    print("Max size population :   " +str(conf.Max_size_population))
    xcs.run_learning()
    print("***************** Evaluation part ******************")
    print("Data set size :   " +str(size_simulation_test))
    
    xcs.run_test()
    #xcs.make_graph()
