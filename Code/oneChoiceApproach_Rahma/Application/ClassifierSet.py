import random
from Config import *
from Environment import *
from Classifier import *
#import XCSClassifier

class ClassifierSet:
    def __init__(self,state,actual_time):
        self.env = state
        self.actual_time = actual_time
        self.clSet = []
    def deep_copy(self):
        classe = ClassifierSet(self.state,self.actual_time)
        classe.cls = self.clSet
        return classe

    def accuracy_sum(self):
        return sum(cl.get_kappa()*cl.numerosity for cl in self.clSet)

    def fitness_sum(self):
        return sum(cl.fitness for cl in self.clSet)

    def numerosity_sum(self):
#        print("taille list:   "+ str(len(self.cls)))
        return sum(cl.numerosity for cl in self.clSet)

    def ts_num_sum(self):
        return sum(cl.time_stamp*cl.numerosity for cl in self.clSet)
    def error_sum(self):
        return sum(cl.error for cl in self.clSet)


    """
        Deletes a classifier from the population, if necessary
    """
    def delete_from_population(self):
        numerosity_sum = self.numerosity_sum()
        if numerosity_sum <= conf.Max_size_population:
            return
        ave_fitness = self.fitness_sum()/float(self.numerosity_sum())
        vote_sum = sum(cl.deletion_vote(ave_fitness) for cl in self.clSet)
        choice_point = vote_sum * random.random()
        vote_sum = 0.0
        i = 0
        for cl in self.clSet:
            vote_sum += cl.deletion_vote(ave_fitness)
            if vote_sum > choice_point:
                cl.numerosity -= 1
                if cl.numerosity == 0:
                    self.remove_classifier(i)
                return cl
            i += 1
        return None
    def remove_classifier(self,num):
        try:
            del self.clSet[num]
        except IndexError:
            raise
        return True
    def remove_classifier_by_instance(self,cl):
        try:
            self.clSet.remove(cl)
            return True
        except ValueError:
            raise

    """
        Inserts the given classifier into the population, if it isn't able to be
        subsumed by some other classifier in the population
        @param cl - the classifier to insert
    """
    def insert_in_population(self,cl):
        for c in self.clSet:
            if c.equals(cl):
                c.numerosity += 1
                return
        self.clSet.append(cl)

# for debug
#if __name__ == '__main__':
#     env = Environment()
#     env.set_state()
##     print(env.state)
##     for e in range(20):
#     x = ClassifierSet(env,1)
#     x.insert_in_population(Classifier(["09:00","09:15",17,20,30,50],0))
#     x.insert_in_population(Classifier(["09:00","09:15",17,20,30,50],0))
#     x.insert_in_population(Classifier(["08:00","09:15",17,20,30,50],0))
#     x.insert_in_population(Classifier(["09:00","09:30",17,20,30,50],0))
#     x.insert_in_population(Classifier(["09:00","09:15",17,20,35,50],0))
#     x.insert_in_population(Classifier(["09:00","09:15",17,20,33,50],0))
#     x.insert_in_population(Classifier(["09:00","09:15",18,20,30,50],0))
#     for i in x.cls:
#         print(i.condition)
#         print(i.action)
#         print(i.numerosity)
#     print(x.accuracy_sum())
