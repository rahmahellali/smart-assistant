import pandas as pd
import os


os.chdir("C:/Users/RAHMA/oneChoiceApproach/Application")
#textdata = pd.read_csv('Actions_sous_actions.csv')
#actions_list = textdata['action'].values.tolist()
#num_action = textdata['num_action'].values.tolist()
#sub_action1 = textdata['sub_action1'].values.tolist()
#sub_action2 = textdata['sub_action2'].values.tolist()
#sub_action3 = textdata['sub_action3'].values.tolist()
#num_sub_sction1 = textdata['num_sub_sction1'].values.tolist()
#num_sub_action2 = textdata['num_sub_action2'].values.tolist()
#num_sub_action3 = textdata['num_sub_action3'].values.tolist()



textdata = pd.read_csv('8actions.csv')
actions_list = textdata['action'].values.tolist()
num_action = textdata['num_action'].values.tolist()
Actions_count=len(num_action)

class Config:
    
    Number_of_actions = Actions_count

    Max_size_population = 6000
    max_iterations = 10000


    alpha = 0.1                # The fall of rate in the fitness evaluation
    beta = 0.2                 # The learning rate for updating fitness, prediction, prediction error, and action set size estimate in XCS's classifiers.
    gamma = 0.71
    delta = 0.1                # The fraction of the mean fitness of the population below which the fitness of a classifier may be considered in its vote for deletion.
    proba_mutation = 0.01      #the propability of mutation an allele in the offspring
    nyu = 5                    # Specifies the exponent in the power function for the fitness evaluation.
    proba_crossover = 0.5      #the probability of applying crossover in the GA

    epsilon_0 = 10             #1% du reward 

    theta_ga = 25 # la marge est de 25 à 50
    theta_del = 20
    theta_sub = 20
    min_number_action_in_MatchSet = Actions_count

    p_sharp = 0.33
    #exploration_probability
    p_explr= 1.0

    doGASubsumption = True
    doActionSetSubsumtion = True

XCSConfig = Config()
conf = XCSConfig
