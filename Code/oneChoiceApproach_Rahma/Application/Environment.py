import pandas as pd
import os



"""**********************for training*****************"""

os.chdir("C:/Users/RAHMA/oneChoiceApproach/Application")
textdata2 = pd.read_csv('8actions_DS.csv')
Time = textdata2['Time'].values.tolist()
Temperature_Comedor_Sensor = textdata2['Temperature_Comedor_Sensor'].values.tolist()
Temperature_Exterior_Sensor = textdata2['Temperature_Exterior_Sensor'].values.tolist()
Lighting_Habitacion_Sensor = textdata2['Lighting_Habitacion_Sensor'].values.tolist()
state_window = textdata2['state_window'].values.tolist()
state_bed = textdata2['state_bed'].values.tolist()
real_action = textdata2['real_action'].values.tolist()



class Environment:

    def set_state(self,i):
        self.__state=[]
        time_state = Time[i]
        room_temperature_state = int(Temperature_Comedor_Sensor[i])
        exterior_temperature_state = int(Temperature_Exterior_Sensor[i])
        Room_lighting_state = int(Lighting_Habitacion_Sensor[i])
        window_state = round(state_window[i],2)
        bed_state = round(state_bed[i],2)
        self.__state.append(time_state)
        self.__state.append(room_temperature_state)
        self.__state.append(exterior_temperature_state)
        self.__state.append(Room_lighting_state)
        self.__state.append(window_state)
        self.__state.append(bed_state)


    def get_state(self):
        return self.__state
    state = property(get_state)



    def is_true(self,proposed_action,i):
        if proposed_action== real_action[i]:
            return True
        else:
            return False
