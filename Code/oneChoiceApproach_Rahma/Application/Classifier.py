import math
from Config import *

from random import randint
import Config
from scipy import spatial
from collections import Counter
from datetime import datetime

"""
A classifier in the X Classifier System
"""

class Classifier:
    def __init__(self,condition,actual_time):

        self.condition = condition[:]
        self.time_stamp = actual_time
        self.numerosity = 1
        self.action_set_size = 1
        self.action = conf.Number_of_actions
        self.prediction = 0.01
        self.error = 0.01
        self.fitness = 0.01
        self.experience=0.0


    def deep_copy(self,actual_time):
        cl = Classifier(self.condition,actual_time)
        cl.action = self.action
        cl.prediction = self.prediction
        cl.error = self.error
#        cl.fitness = self.fitness/self.numerosity
        cl.fitness = self.fitness
        cl.numerosity = self.numerosity
        cl.experience = self.experience
        cl.time_stamp = actual_time
        cl.action_set_size = self.action_set_size
        return cl

    def __str__(self):
        return ("Classifier : " + self.condition + " = " + str(self.action) + " Fitness: " + str(self.fitness) + " Prediction: " + str(self.prediction) + " Error: " + str(self.error) + " Experience: " + str(self.experience))

    def update_fitness(self,acc_sum):
        self.fitness += conf.beta*(self.get_kappa()*self.numerosity/acc_sum-self.fitness)


    def update_parameters(self,reward,num_sum):
        self.experience += 1
        '''update prediction'''
        if self.experience < (1/conf.beta):
            self.prediction += (reward-self.prediction)/self.experience
        else:
            self.prediction += conf.beta*(reward-self.prediction)
        '''update prediction error'''
        if self.experience < (1/conf.beta):
            self.error += (math.fabs(reward-self.prediction)-self.error)/self.experience
        else:
            self.error += conf.beta*(math.fabs(reward-self.prediction)-self.error)
        '''update action set size estimate '''
        if self.experience < (1/conf.beta):
            self.action_set_size += (num_sum-self.action_set_size)/self.experience
        else:
            self.action_set_size += conf.beta*(num_sum-self.action_set_size)


    def deletion_vote(self,ave_fitness):
        vote = self.action_set_size*self.numerosity
        if self.experience > conf.theta_del:
            if self.fitness/self.numerosity < conf.delta*ave_fitness:
                vote *= ave_fitness/(self.fitness/self.numerosity)
        return vote


    def equals(self,cl):
        if self.condition == cl.condition:
            if self.action == cl.action:
                return True
        return False


    def does_subsume(self,cl_tos):
        if self.action == cl_tos.action:
            if self.could_subsume() and self.is_more_general(cl_tos):
                return True
        return False


    def could_subsume(self):
        if self.experience > conf.theta_sub and self.error < conf.epsilon_0:
            return True
        return False



    def pourcentage_number_intersection(self,interval1,interval2):
        #si l'interval est de type integer or float
        #cette fontction calcul le pourcentage d'intersection entre deux intervall mis en paramètre de la méthode
        borne_inf_interval1 = int(min(interval1))
        borne_sup_interval1 = int(max(interval1))
        borne_inf_interval2 = int(min(interval2))
        borne_sup_interval2 = int(max(interval2))
        ecart_interval = (borne_sup_interval1 - borne_inf_interval1) + 1
            #test if there is intersection
        intersection= []
        if borne_sup_interval1 < borne_inf_interval2 or borne_inf_interval1 > borne_sup_interval2:
            return 0
        if borne_inf_interval1 == borne_inf_interval2 and borne_sup_interval1 == borne_sup_interval2:

            intersection.append(borne_inf_interval1)
            intersection.append(borne_sup_interval1)
        else:
            if borne_sup_interval1 <= borne_sup_interval2 or (borne_sup_interval1 < borne_sup_interval2 and borne_inf_interval1 == borne_inf_interval2):

                intersection.append(borne_inf_interval2)
                intersection.append(borne_sup_interval1)
            elif borne_sup_interval1 >= borne_sup_interval2 or (borne_sup_interval1 > borne_sup_interval2 and borne_inf_interval1 == borne_inf_interval2):
                intersection.append(borne_inf_interval1)
                intersection.append(borne_sup_interval2)
        #ecart interval d'intersection
        b_inf_inter = intersection[0]
        b_sup_inter = intersection[1]
        ecart_inter = (b_sup_inter-b_inf_inter) + 1
        pourcentage_num_intersection = ecart_inter / ecart_interval
        return pourcentage_num_intersection

#    interval_intersection = intervals.Interval(interval1) & intervals.Interval(interval2)
#    return interval_intersection

    def convertir_time_to_integer(self,t1,t2,t3,t4):
                
        h_t1 , m_t1 = t1.split(":")
        borne_t1 = int(str(h_t1)+str(m_t1))
                    
        h_t2 , m_t2 = t2.split(":")
        borne_t2 = int(str(h_t2)+str(m_t2))
        
        h_t3 , m_t3 = t3.split(":")
        borne_t3 = int(str(h_t3)+str(m_t3))
                
        h_t4 , m_t4 = t4.split(":")
        borne_t4 = int(str(h_t4)+str(m_t4))
                
        return borne_t1,borne_t2,borne_t3,borne_t4 
            
    def convert_to_time(self,time_borne):
        if time_borne < 1000:
            time_borne = str(time_borne)
            time_borne = "0" + time_borne[0] + ":" + time_borne[1] + time_borne[2]
            return time_borne
        elif time_borne >= 1000 :
            time_borne = str(time_borne)
            time_borne = time_borne[0] + time_borne[1] + ":" + time_borne[2] + time_borne[3]
        return time_borne


    def pourcentage_time_intersection(self,time_interval1,time_interval2):
        #si l'interval de type time cad str(chaine de charactère)
        #cette méthode permet de calculer le pourcentage d'intersection entre deux intervall
        borne_inf_time_interval1 = str(time_interval1[0])
        borne_sup_time_interval1 = str(time_interval1[1])
        borne_inf_time_interval2 = str(time_interval2[0])
        borne_sup_time_interval2 = str(time_interval2[1])
        intersection_time=[]

        t1_inf,t2_sup,t3_inf,t4_sup = self.convertir_time_to_integer(borne_inf_time_interval1,borne_sup_time_interval1,borne_inf_time_interval2,borne_sup_time_interval2)

        if t2_sup < t3_inf or t1_inf > t4_sup:
            return 0
        elif t1_inf==t3_inf and t2_sup==t4_sup:
            a = self.convert_to_time(t1_inf)
            u = self.convert_to_time(t2_sup)

            intersection_time.append(a)
            intersection_time.append(u)
        else:
            if t2_sup <= t4_sup or (t2_sup < t4_sup and t1_inf == t3_inf):
                o = self.convert_to_time(t3_inf)
                p = self.convert_to_time(t2_sup)

                intersection_time.append(o)
                intersection_time.append(p)
            elif t2_sup >= t4_sup or (t2_sup > t4_sup and t1_inf == t3_inf):
                n = self.convert_to_time(t1_inf)
                l = self.convert_to_time(t4_sup)

                intersection_time.append(n)
                intersection_time.append(l)

        s1 = intersection_time[0]
        s2 = intersection_time[1]
        FMT = '%H:%M'
        tdelta = datetime.strptime(s2, FMT) - datetime.strptime(s1, FMT)
        tdelta=str(tdelta)
        (h, m, s) = tdelta.split(':')
        ecart_time = int(h) * 60 + int(m)+ int(s)*0
        pourcentage_t_intersection=ecart_time/60
        return pourcentage_t_intersection


    def is_more_general(self,cl_spec):
        int1 = [self.condition[0],self.condition[1]]
        int2 = [self.condition[2],self.condition[3]]
        int3 = [self.condition[4],self.condition[5]]
        int4 = [self.condition[6],self.condition[7]]
        int5 = [self.condition[8],self.condition[9]]
        int6 = [self.condition[10],self.condition[11]]

        inter1 = [cl_spec.condition[0],cl_spec.condition[1]]
        inter2 = [cl_spec.condition[2],cl_spec.condition[3]]
        inter3 = [cl_spec.condition[4],cl_spec.condition[5]]
        inter4 = [cl_spec.condition[6],cl_spec.condition[7]]
        inter5 = [cl_spec.condition[8],cl_spec.condition[9]]
        inter6 = [cl_spec.condition[10],cl_spec.condition[11]]
        
        
        
        res_int1 = self.pourcentage_time_intersection(int1,inter1)
        res_int2 = self.pourcentage_number_intersection(int2,inter2)
        res_int3 = self.pourcentage_number_intersection(int3,inter3)
        res_int4 = self.pourcentage_number_intersection(int4,inter4)
        res_int5 = self.pourcentage_number_intersection(int5,inter5)
        res_int6 = self.pourcentage_number_intersection(int6,inter6)

#        somme = res_int1 + res_int2 + res_int3 + res_int4 + res_int5 + res_int6
#        resultat=somme/6

        
        liste_resultat_intersection=[res_int1, res_int2, res_int3, res_int4, res_int5, res_int6]
        nombre_interval_intersection = 0
        
        for i in range(len(liste_resultat_intersection)):
            if liste_resultat_intersection[i]!=0:
                nombre_interval_intersection+=1
        
        
        if nombre_interval_intersection > 3 :
            return True
        else:
            return False
        
        #compter le nombre d'interval qui ont une intersection
    #    i=0
    #    if res_int1 != 0  :
    #        i+=1
    #    if res_int2 != 0 :
    #        i+=1
    #    if res_int3 != 0 :
    #        i+=1
    #    if res_int4 != 0 :
    #        i+=1

    def get_kappa(self):
        kappa = 0.0
        if self.error < conf.epsilon_0:
            kappa = 1.0
        else:
            kappa = conf.alpha * math.pow(self.error/conf.epsilon_0,-conf.nyu)
        return kappa


# for debug
#if __name__ == '__main__':
#    a = ['08:05']
#    a1 = ['17:05']
#    x=Classifier(["09:00","09:15",17,20,30,50],0)
   
 

#t = "10:30"
#(h, m) = t.split(':')
#print(h)
#print(m)
#r = int(str(h) + str(m))
#print(r)
#p = intersection_interval([2,9],[7,8])
#print(p)
#
#s1 = '10:20'
#s2 = '11:30'
#FMT = '%H:%M'
#tdelta = datetime.strptime(s2, FMT) - datetime.strptime(s1, FMT)
#
#print(tdelta)

#ss=pourcentage_number_intersection([1,5],[1,5])
#print(ss)
#m = pourcentage_time_intersection(['09:20','10:20'],['09:20','10:20'])
#print(m)
#t= is_more_general(['09:15','10:15',12,18,10,16,130,150],['09:30','10:30',11,15,5,11,39,59])
#print(t)



